# Model-driven Privacy

## Paper

    Model-driven Privacy
    Srdan Krstic, Hoang Nguyen, and David Basin
    In: Proc. Priv. Enhancing Technol. 2024(1)


## Description

This open-source project is intended for readers of our [paper](https://petsymposium.org/popets/2024/popets-2024-0018.pdf).

## Prototypes materials

This folder is structured as follows:
  * `cs_generator` is the prototype of our model-driven privacy approach targeting C#.
    * `config` stores some global configurations for running the prototype. These configurations are stored in a JSON file. Currently, existing configurations include value related to the values of the Visitor role (i.e., their role name, ID in the GUID format) as well as contact email placeholder for security and privacy problems and concerns.
    * `models` includes different set of models of the different domains
      * `ConfMS`: a Conference Management System,
      * `Hipaa`: a Health Record Manager, and
      * `MiniTwitPlus`: a Social Networking Platform.
    * `resources` contains the templates that will be used to generate C#/ASP.NET application. These templates are written in [Jinja](https://jinja.palletsprojects.com/en/3.1.x/) templating engine.
    * `output` contains the generated applications.
    * `scripts` stores the scripts to run the model-to-code generation.
  * `py_generator` is the prototype of our model-driven privacy approach targeting Python.
    * `models` includes the similar set of models mentioned above.
    * `resources` contains the templates that will be used to generate Python/Flask application. These templates are also written in Jinja.

## Getting started

## Steps to Use the prototype

To make it easy for interested readers to build applications of your own domain, here are the recommended next steps.

### Choose a target technology
  * For generating a Flask/Python application, work in the `py_generator` directory.
  * For generating an ASP.NET/C# application, work in the `cs_generator` directory.

### Add relevant models:
  * In the `models` sub-directory, create a directory with a name of your choice (e.g., `my-domain`).
  * Define the following models for your application: data model, security model, and privacy model. 

### Generate code and Deploy:
  * In the `scripts` sub-directory, run the bash script with your model folder name as an argument 
```
run.sh my-domain
```
  * The application will be generated and located in `output` folder.

## Contributing
We thank Francois Hublet for helping us review the tool during its first development iteration.

## Authors and acknowledgment
Srdan Krstic and Hoang Nguyen.

For questions and others, please sent to [Hoang Nguyen](mailto:hoang.nguyen@inf.ethz.ch?subject=Question%20on%20the%20mdp%20prototype").

## License
The Model-driven Privacy repository is licensed under MIT License.

## Project status
The project is currently ongoing. 

We are improving the model transformations and adding more examples.


