﻿// (c) 2023 Anonymous Author(s)
// Generated code

using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using {{project}}.Models;
using Microsoft.AspNetCore.Diagnostics;
using Newtonsoft.Json;

namespace {{project}}.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error(string message="")
    {
        var errorMessages = JsonConvert.DeserializeObject<ErrorViewModel>(message);
        return View(errorMessages);
    }
}
