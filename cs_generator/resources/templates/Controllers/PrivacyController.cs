﻿// (c) 2023 Anonymous Author(s)
// Generated code

using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using {{project}}.Data;
using {{project}}.Models;

namespace {{project}}.Controllers
{
    public class PrivacyController : Controller
    {
        private readonly ApplicationDbContext _context;

        public PrivacyController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            User? currentUser = Context.GetCurrentUser();
            List<Policy> PolicyList = Context.getPolicy();
            PolicyList.ForEach(policy =>
            {
                bool decision = true;
                foreach (string pdt in policy.personalDataTypes)
                {
                    List<Consent> consents = Context.context.Consent.Where(c => c.user == currentUser
                                                                             && c.data == pdt
                                                                             && c.purpose == policy.purpose).ToList();
                    if (consents.Count == 0)
                    {
                        decision = false;
                        break;
                    }
                }
                policy.decision = decision;
                policy.setData();
            });
            return View(PolicyList);
        }

        public async Task<IActionResult> Accept(Policy policy)
        {
            User? user = Context.GetCurrentUser();
            Context.accept(user, Context.getPolicy().Where(p => p.Id == policy.Id).First());
            return RedirectToAction(nameof(Index), "Privacy");
        }

        public async Task<IActionResult> Revoke(Policy policy)
        {
            User? user = Context.GetCurrentUser();
            Context.revoke(user, Context.getPolicy().Where(p => p.Id == policy.Id).First());
            return RedirectToAction(nameof(Index), "Privacy");
        }

        public ActionResult Return()
        {
            return RedirectToAction(nameof(Index), "Home");
        }

    }
}