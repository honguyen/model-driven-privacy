﻿// (c) 2023 Anonymous Author(s)
// Generated code

using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using {{project}}.Privacy;
using {{project}}.Security;
using {{project}}.Models;

namespace {{project}}.Data
{
    public class ApplicationDbContext : IdentityDbContext<User,Role,Guid>
{
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            Context.context = this;
        }

        {%- for c in data %}
        {%- if "isAssociation" not in c or not c.isAssociation %}
        public DbSet<{{c.class}}> {{c.class}} { get; set; }
        {%- endif %}
        {%- endfor %}
        public DbSet<Consent> Consent { get; set; }
        public DbSet<PersonalData> PersonalData { get; set; }
        public DbSet<Purpose> Purpose { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Role> Role { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            {%- for c in data %}
            {%- if "attributes" in c %}
            {%- for att in c.attributes %}
            {%- if not is_primitive(att.type) %}
            {%- if not att.type == "PersonalData" or "isAssociation" not in c or not c.isAssociation %}
            modelBuilder.Entity<{{c.class}}>()
            .HasOne(e => e.{{att.name}});
            {%- endif %}
            {%- endif %}
            {%- endfor %}
            {%- endif %}
            {%- if "isAssociation" in c and c.isAssociation %}
            modelBuilder.Entity<{{c.ends[0].target}}>()
            .{% if c.ends[1].mult == "*" %}HasMany{% else %}HasOne{% endif %}(e => e._{{c.ends[1].name}}{% if c.ends[1].mult == "*" %}{% endif %})
            .{% if c.ends[0].mult == "*" %}WithMany{% else %}WithOne{% endif %}(e => e._{{c.ends[0].name}}{% if c.ends[0].mult == "*" %}{% endif %})
            .UsingEntity("{{c.class}}",
                {%- if c.ends[0].target == c.ends[1].target %}
                l => l.HasOne(typeof({{c.ends[0].target}})).{% if c.ends[0].mult == "*" %}WithMany{% else %}WithOne{% endif %}().HasForeignKey("_{{c.ends[0].name}}Id").HasPrincipalKey(nameof({{project}}.Models.{{c.ends[0].target}}.Id)),
                r => r.HasOne(typeof({{c.ends[1].target}})).{% if c.ends[1].mult == "*" %}WithMany{% else %}WithOne{% endif %}().HasForeignKey("_{{c.ends[1].name}}Id").HasPrincipalKey(nameof({{project}}.Models.{{c.ends[1].target}}.Id)),
                {%- endif %}
                {%- if is_personalData(c.class,privacy) %}
                j => j.HasOne(typeof(PersonalData), "data")
                {%- else %}
                j => j.HasKey("_{{c.ends[1].name}}Id", "_{{c.ends[0].name}}Id")
                {%- endif %}
                );
            {%- endif %}
            {%- endfor %}
            modelBuilder.ApplyConfiguration(new RoleConfiguration());
            modelBuilder.ApplyConfiguration(new VisitorUserConfiguration());
            modelBuilder.ApplyConfiguration(new VisitorUserRoleConfiguration());
            modelBuilder.ApplyConfiguration(new PurposeConfiguration());
        }
    }
}