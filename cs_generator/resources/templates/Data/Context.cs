﻿// (c) 2023 Anonymous Author(s)
// Generated code

using {{project}}.Models;
using System.Security.Claims;

namespace {{project}}.Data
{
    public class Context
    {
        public static ApplicationDbContext? context;
        private const string VISITORUSER = "{{ config.Visitor.UserID }}";
        public static User GetCurrentUser()
        {
            var httpContext = new HttpContextAccessor().HttpContext;
            var users = httpContext?.User.Claims.Where(c => c.Type == ClaimTypes.NameIdentifier).ToList();
            if (users == null || users.Count == 0)
            {
                return GetVisitorUser();
            }
            else
            {
                var currentUserId = users.ElementAt(0).Value;
                Guid guid = new Guid(currentUserId);
                return context.User.Where(u => u.Id == guid).First();
            }
        }

        public static User GetVisitorUser()
        {
            Guid user = new Guid(VISITORUSER);
            return context.User.Where(u => u.Id == user).First();
        }

        public static Role? GetRole(User caller)
        {
            var roleIds = context.UserRoles.Where(ur => ur.UserId == caller.Id).Select(ur => ur.RoleId).ToList();
            if (roleIds == null || roleIds.Count == 0)
            {
                return null;
            }
            else
            {
                var roleId = roleIds.ElementAt(0);
                return context.Roles.Where(r => r.Id == roleId).First();
            }
        }

        public static Purpose getPurpose(string purpose)
        {
            List<Purpose> purposes = context.Purpose.ToList();
            List<Purpose> myPurposes = purposes.Where(p => p.purpose == purpose).ToList();
            if (myPurposes.Count > 0)
            {
                return myPurposes[0];
            }
            throw new Exception("There is no purpose declared with name " + purpose + "!");
        }

        public static PersonalData getPersonalData(string personalDatum)
        {
            List<PersonalData> personalData = context.PersonalData.ToList();
            List<PersonalData> myPersonalData = personalData.Where(pd => pd.name == personalDatum).ToList();
            if (myPersonalData.Count > 0)
            {
                return myPersonalData[0];
            }
            throw new Exception("There is no personal data declared with name " + personalDatum + "!");
        }

        public static List<Policy> getPolicy()
        {
            List<Policy> policies = new List<Policy>();
                        
            {%- for p in privacy.policy %}
            {% set outer_loop = loop %}
            Policy policy{{ outer_loop.index }} = new Policy();
            policy{{ outer_loop.index }}.Id = {{ outer_loop.index }};
            policy{{ outer_loop.index }}.purpose = getPurpose("{{p.purpose}}");
            policy{{ outer_loop.index }}.desc = "{{p.constraint.desc}}";
            SortedSet<string> personalDataTypes{{ outer_loop.index }} = new SortedSet<string>
            {
                {%- for r in p.resources %}
                "{{r.class}}"{% if not loop.last %},{% endif %}
                {%- endfor %}
            };
            policy{{ outer_loop.index }}.personalDataTypes = personalDataTypes{{ outer_loop.index }};
            policies.Add(policy{{ outer_loop.index }});
            {%- endfor %}
            return policies;
        }

        public static void accept(User currentUser, Policy policy)
        {
            SortedSet<string> personalDataTypes = policy.personalDataTypes;
            foreach (string pdt in personalDataTypes)
            {
                addConsent(currentUser, pdt, policy.purpose);
            }
            context.SaveChanges();
        }

        private static void addConsent(User currentUser, string data, Purpose purpose)
        {
            context.Consent.Add(new Consent { user = currentUser, data = data, purpose = purpose });
            List<Purpose> childPurposes = context.Purpose.Where(p => p.Parent == purpose).ToList();
            foreach (Purpose p in childPurposes)
            {
                addConsent(currentUser, data, p);
            }
        }

        public static void revoke(User currentUser, Policy policy)
        {
            SortedSet<string> personalDataTypes = policy.personalDataTypes;
            foreach (string pdt in personalDataTypes)
            {
                revokeConsent(currentUser, pdt, policy.purpose);
            }
            context.SaveChanges();
        }

        private static void revokeConsent(User currentUser, string data, Purpose purpose)
        {
            List<Consent> myConsents = context.Consent.Where(c => c.user == currentUser
                                                                  && c.purpose == purpose
                                                                  && c.data == data).ToList();
            foreach (Consent consent in myConsents)
            {
                context.Consent.Remove(consent);
            }
            if (purpose.Parent != null)
            {
                revokeConsent(currentUser, data, purpose.Parent);
            }
        }
    }
}
