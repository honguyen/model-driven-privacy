// (c) 2023 Anonymous Author(s)
// Generated code

using {{ project }}.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace {{ project }}.Data
{
	public class PurposeConfiguration : IEntityTypeConfiguration<Purpose>
	{
		
		public void Configure(EntityTypeBuilder<Purpose> builder)
		{
			{%- for p in privacy.purposes %}
			Purpose {{p.name}} = new Purpose { Id = {{ loop.index }}, purpose = "{{ p.name }}" };
			{%- endfor %}
			{%- for p in privacy.purposes %}
			{%- if 'includes' in p and p.includes != [] %}
			{% for s in p.includes %}{{s}}.ParentId = {{p.name}}.Id;{% endfor %}
			{%- endif %}
			{%- endfor %}
			builder.HasData
            (
                {%- for p in privacy.purposes %}
				{{p.name}}{% if not loop.last %},{% endif %}
                {%- endfor %}
            );
		}
	}
}