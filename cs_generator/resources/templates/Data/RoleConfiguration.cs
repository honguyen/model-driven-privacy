// (c) 2023 Anonymous Author(s)
// Generated code

using {{ project }}.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace {{ project }}.Data
{
	public class RoleConfiguration : IEntityTypeConfiguration<Role>
	{
		private const string VISITORROLE = "{{ config.Visitor.RoleID }}";
		{%- for r in security.roles %}
		private Guid {{r.name}}ROLE = Guid.NewGuid();
		{%- endfor %}
		public void Configure(EntityTypeBuilder<Role> builder)
		{
			{%- for r in security.roles %}
			Role {{r.name}} = new Role { Id = {{r.name}}ROLE, Name = "{{r.name}}", NormalizedName = "{{r.name}}".ToUpper()};
			{%- endfor %}
			{%- for r in security.roles %}
			{%- if 'extends' in r and r.extends != [] %}
			{% for s in r.extends %}{{s}}.ParentId = {{r.name}}ROLE;{% endfor %}
			{%- endif %}
			{%- endfor %}
			builder.HasData(
                new Role { Id = new Guid(VISITORROLE), Name = "{{ config.Visitor.Name }}", NormalizedName = "{{ config.Visitor.Name }}".ToUpper() },
				{%- for r in security.roles %}
                {{r.name}}{% if not loop.last %},{% endif %}
                {%- endfor %}
			);
		}
	}
}