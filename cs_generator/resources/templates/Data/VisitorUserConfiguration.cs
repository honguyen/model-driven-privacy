// (c) 2023 Anonymous Author(s)
// Generated code

using {{ project }}.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace {{ project }}.Data
{
	public class VisitorUserConfiguration : IEntityTypeConfiguration<User>
	{
        private const string VISITORUSER = "{{config.Visitor.UserID}}";
		public void Configure(EntityTypeBuilder<User> builder)
		{
			var visitor = new User
            {
                Id = new Guid(VISITORUSER),
                UserName = "{{ config.Visitor.Name }}",
                NormalizedUserName = "{{ config.Visitor.Name }}".ToUpper()
            };

            visitor.PasswordHash = PassGenerate(visitor);

            builder.HasData(visitor);
		}

		public string PassGenerate(User user)
        {
            var passHash = new PasswordHasher<User>();
            return passHash.HashPassword(user, "{{ config.Visitor.Password }}");
        }
	}
}