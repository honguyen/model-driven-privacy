// (c) 2023 Anonymous Author(s)
// Generated code

using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace {{ project }}.Data
{
	public class VisitorUserRoleConfiguration : IEntityTypeConfiguration<IdentityUserRole<Guid>>
	{
        private const string VISITORUSER = "{{ config.Visitor.UserID }}";
        private const string VISITORROLE = "{{ config.Visitor.RoleID }}";
		public void Configure(EntityTypeBuilder<IdentityUserRole<Guid>> builder)
        {
            IdentityUserRole<Guid> iur = new IdentityUserRole<Guid>
            {
                RoleId = new Guid(VISITORROLE),
                UserId = new Guid(VISITORUSER)
            };

            builder.HasData(iur);
        }
	}
}