﻿// (c) 2023 Anonymous Author(s)
// Generated code

using {{project}}.Privacy;
using Newtonsoft.Json;
using System.Net;

namespace {{project}}.Error
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        private readonly ILogger _logger;
        public ErrorHandlingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            this.next = next;
            _logger = loggerFactory.CreateLogger<ErrorHandlingMiddleware>();
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static async Task HandleExceptionAsync(HttpContext context, Exception ex)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            var result = JsonConvert.SerializeObject(new
            {
                Status = "Internal Error",
                ErrorCode = (int) HttpStatusCode.InternalServerError,
                ErrorMessage = "An error occurred on the server.",
                ErrorSubMessage = "We apologize for any inconvenience this may have caused."
            });
            if (ex is UnauthorizedAccessException)
            {
                UnauthorizedAccessException unauthex = ex as UnauthorizedAccessException; 
                result = JsonConvert.SerializeObject(new
                {
                    Status = unauthex.Status,
                    ErrorCode = unauthex.Code,
                    ErrorMessage = unauthex.Message,
                    ErrorSubMessage = unauthex.SubMessage
                });
            }
            if (ex is PrivacyException)
            {
                PrivacyException privex = ex as PrivacyException;
                result = JsonConvert.SerializeObject(new
                {
                    Status = privex.Status,
                    ErrorCode = privex.Code,
                    ErrorMessage = privex.Message,
                    ErrorSubMessage = privex.SubMessage
                });
            }
            
            await HandleErrorAsync(context, result);
        }

        private static async Task HandleErrorAsync(HttpContext context, string result)
        {
            context.Response.Redirect($"/Home/Error?message={result}");
        }
    }
}
