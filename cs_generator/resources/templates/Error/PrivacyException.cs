﻿// (c) 2023 Anonymous Author(s)
// Generated code

using System.Net;

namespace {{project}}.Error
{
    [Serializable]
    public class PrivacyException : Exception
    {
        public readonly int Code = (int)HttpStatusCode.Unauthorized;
        public readonly string Status = "Privacy Exception";
        public string SubMessage = String.Empty;

        public PrivacyException() : base() { }
        public PrivacyException(string message) : base(message) { }
        public PrivacyException(string message, Exception inner) : base(message, inner) { }
    }
}
