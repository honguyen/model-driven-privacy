﻿// (c) 2023 Anonymous Author(s)
// Generated code

using System.Net;

namespace {{project}}.Error
{
    [Serializable]
    public class UnauthorizedAccessException : Exception
    {
        public readonly int Code = (int)HttpStatusCode.Unauthorized;
        public readonly string Status = "Unauthorized Access";
        public string SubMessage = String.Empty;

        public UnauthorizedAccessException() : base() { }
        public UnauthorizedAccessException(string message) : base(message) { }
        public UnauthorizedAccessException(string message, Exception inner) : base(message, inner) { }
    }
}
