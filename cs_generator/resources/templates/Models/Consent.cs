// (c) 2023 Anonymous Author(s)
// Generated code

using System.ComponentModel.DataAnnotations;

namespace {{project}}.Models
{
    public class Consent
    {
        [Key]
        public int Id { get; set; }

        public User? user { get; set; }

        public string? data { get; set; }

        public Purpose? purpose { get; set; }

        public Consent () { }
    }
}