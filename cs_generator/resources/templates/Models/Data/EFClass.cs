// (c) 2023 Anonymous Author(s)
// Generated code

using System.ComponentModel.DataAnnotations;
using {{project}}.Security;
{%- if is_personalData(c.class,privacy) %}using {{project}}.Privacy;{% endif %}
using {{project}}.Data;
using Microsoft.EntityFrameworkCore;

namespace {{project}}.Models
{
    {% if is_personalData(c.class,privacy) %}[PersonalData]{%- endif %}
    public class {{c.class}}
    {
        [Key]
        public int Id { get; set; }
        {%- if "attributes" in c %}
        {%- for att in c.attributes %}
        {% if is_primitive(att.type) %}
        [SecuredAttribute]
        public {{type(att.type)}}? {{att.name}} { get; set; }
        {% else %}
        public {{att.type}}? {{att.name}} { get; set; }
        {%- endif %}
        {%- endfor %}
        {%- endif %}
        {%- if "ends" in c %}
        {%- for end in c.ends %}
        {% if end.mult == "*" %}
        public List<{{end.target}}> _{{end.name}} = new();

        [SecuredEndMethod]
        public List<{{end.target}}> {{end.name}}()
        {
            {{c.class}} o = Context.context.{{c.class}}.Include(o => o._{{end.name}}).Where(o => o.Id == Id).FirstOrDefault();
            return o._{{end.name}};
        }

        [SecuredEndMethod]
        public void Add{{end.name}}({{end.target}} {{end.name}})
        {
            _{{end.name}}.Add({{end.name}});
        }

        [SecuredEndMethod]
        public void Remove{{end.name}}({{end.target}} {{end.name}})
        {
            _{{end.name}}.Remove({{end.name}});
        }
        {% else %}
        public {{end.target}}? _{{end.name}} { get; set; }
        {% endif %}
        {%- endfor %}
        {%- endif %}
        public {{c.class}} () 
        { }
        {% for met in c.methods %}
        /// <summary>
        /// {{met.desc}}
        /// </summary>
        {%- for param in met.params %}
        /// <param name="{{param.name}}">{{param.desc}}</param>
        {%- endfor %}
        {%- if "return" in met %}
        /// <returns>{{met.return.desc}}</returns>
        {%- endif %}
        [SecuredMethod]
        public {% if "return" in met %}{{met.return.type}}{% else %}void{% endif %} {{met.name}} ({{params(met.params)}}) 
        { 
            // TODO: Implement method stubs
            return {% if "return" in met %}null{% else %}{% endif %};
        }
        {% endfor %}

        [SecuredMethod]
        public void Create()
        {
            {%- if is_personalData(c.class,privacy) %}
            PersonalData data = new PersonalData();
            data.name = this.GetType().Name;
            this.data = data;
            {%- endif %}
        }
        
        [SecuredMethod]
        public void Delete()
        {
            RemoveInContext();
        }

        private void RemoveInContext()
        {
            Context.context.{{c.class}}.Remove(this);
            Context.context.SaveChanges();
        }

        private void UpdateInContext()
        {
            if (Context.context.Entry(this).State == EntityState.Added)
            {
                Context.context.{{c.class}}.Add(this);

            }
            else
            {
                Context.context.{{c.class}}.Update(this);
            }
            Context.context.SaveChanges();
        }
    }
}
