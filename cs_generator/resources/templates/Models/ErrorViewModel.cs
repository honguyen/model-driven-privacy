// (c) 2023 Anonymous Author(s)
// Generated code

namespace {{project}}.Models;

public class ErrorViewModel
{
    public string? Status {get; set;}
    public int? ErrorCode { get; set;}
    public string? ErrorMessage { get; set;}
    public string? SubErrorMessage { get; set; }
}
