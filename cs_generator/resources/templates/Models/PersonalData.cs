// (c) 2023 Anonymous Author(s)
// Generated code

using System.ComponentModel.DataAnnotations;

namespace {{project}}.Models
{
    public class PersonalData
    {
        [Key]
        public int Id { get; set; }

        public string? name { get; set; }

        public User? owner { get; set; }
        
        public PersonalData () { }
    }
}