﻿// (c) 2023 Anonymous Author(s)
// Generated code

namespace {{project}}.Models
{
    public class Policy
    {
        public int Id { get; set; }
        public Purpose purpose { get; set; }
        public SortedSet<string> personalDataTypes { get; set; }
        public List<Consent> consents { get; set; }
        public string desc { get; set; }

        public bool decision { get; set; }

        public string data { get; set; }

        public Policy() { }

        public void setData()
        {
            data = string.Empty;
            foreach (string pdt in personalDataTypes)
            {
                data += pdt + ", ";
            }
            data = data.Substring(0, data.Length - 2);
        }
    }
}