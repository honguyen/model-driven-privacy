// (c) 2023 Anonymous Author(s)
// Generated code

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace {{project}}.Models
{
    public class Purpose
    {
        [Key]
        public int Id { get; set; }

        public string? purpose { get; set; }
        
        [ForeignKey("Parent")]
        public int? ParentId { get; set; }
        public virtual Purpose? Parent { get; set; }

        public Purpose () { }
    }
}