// (c) 2023 Anonymous Author(s)
// Generated code

using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace {{project}}.Models
{
    public class Role : IdentityRole<Guid>
    {
        [ForeignKey("Parent")]
        public Guid? ParentId { get; set; }
        public virtual Role? Parent { get; set; }
    }
}