﻿// (c) 2023 Anonymous Author(s)
// Generated code

namespace {{ project }}.Privacy
{
    [AttributeUsage(AttributeTargets.Class |
                           AttributeTargets.Struct,
                           AllowMultiple = true)  // multiuse attribute  
    ]
    public class PersonalDataAttribute : Attribute
    {
    }
}