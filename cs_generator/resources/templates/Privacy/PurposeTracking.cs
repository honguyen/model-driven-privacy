﻿// (c) 2023 Anonymous Author(s)
// Generated code

using {{project}}.Data;
using {{project}}.Models;
using {{project}}.Error;
using Microsoft.EntityFrameworkCore;

namespace {{project}}.Privacy
{
    public class PurposeTracking
{
        public static Stack<Purpose> actualPurposes = new Stack<Purpose>();
        public static List<Policy> violatedPolicies = new List<Policy>();

        public static void push(string purpose)
        {
            List<Purpose> purposes = Context.context.Purpose.Where(p => p.purpose == purpose).ToList();
            foreach(Purpose p in purposes)
            {
                actualPurposes.Push(p);
            }
        }

        public static void pop(string purpose)
        {
            List<Purpose> purposes = Context.context.Purpose.Where(p => p.purpose == purpose).ToList();
            foreach(Purpose p in purposes)
            {
                actualPurposes.Pop();
            }
        }

        public static void CheckForConsents(Object o)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!hasGivenConsent(o.GetType(), o, actualPurpose, caller))
                {
                    SortedSet<string> personalDataTypes = new SortedSet<string>
                    {
                        o.GetType().Name
                    };
                    violatedPolicies.Add(new Policy { purpose = actualPurpose, personalDataTypes = personalDataTypes });
                    throw new PrivacyException("The owner does not give consent to perform action on "
                        + o.GetType().Name + " [PersonalData] for " + actualPurpose.purpose + " [purpose].");
            }
            }
        }

        private static bool hasGivenConsent(Type dataType, Object self, Purpose purpose, User? caller)
        {
            PersonalData data = getPersonalData(self);

            if (data != null)
            {
                if (data.name != dataType.Name)
                {
                    return false;
                }
                List<User> owners = Context.context.PersonalData.Where(pd => pd == data).Select(pd => pd.owner).ToList();
                foreach (User owner in owners) 
                {
                    List<Consent> consents = Context.context.Consent.Include(c => c.user)
                    .Include(c => c.purpose).ToList();
                    if (Context.context.Consent.Include(c => c.user).Include(c => c.purpose)
                        .Any(c => c.user == owner && c.purpose == purpose && c.data == dataType.Name))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private static PersonalData getPersonalData(Object o)
        {
            if (o != null)
            {
                {%- for pd in privacy.personalData %}
                if (o.GetType().Name == "{{pd}}")
                {
                    return Context.context.{{ pd }}.Select(r => r.data).Where(r => r.Id == (o as {{ pd }}).Id).FirstOrDefault();
                }
                {%- endfor %}
            }
            throw new Exception("Current accessed object is not a personal data object!");
        }

        // Authorization checks
        public static void PurposeBasedCheckCreateObject(string entity)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!PurposeBasedCheckCreateObject(entity, caller, actualPurpose)) 
                {
                    throw new PrivacyException("The owner does not give consent to create object of type "
                        + entity + " for " + actualPurpose.purpose + " purpose.");
                }
            }
        }

        private static bool PurposeBasedCheckCreateObject(string entity, User? caller, Purpose purpose)
        {
            {%- for p in privacy.policy %}
            {%- if p.action == "create" and "class" in p.resource %}
            if (entity == "{{p.resource.class}}") 
            {
                //TODO: Implement OCL constraint: {{ p.constraint.ocl }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        public static void PurposeBasedCheckReadAttribute(string entity, string attribute, object self)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!PurposeBasedCheckReadAttribute(entity, attribute, self, caller, actualPurpose)) 
                {
                    throw new PrivacyException("The owner does not give consent to read value of " 
                        + attribute + " of " + entity + " for " + actualPurpose.purpose + " purpose.");
                }
            }
        }

        private static bool PurposeBasedCheckReadAttribute(string entity, string attribute, object self, User? caller, Purpose purpose)
        {
            {%- for p in privacy.policy %}
            {%- if p.action == "read" and "class" in p.resource and "attribute" in p.resource %}
            if (entity == "{{p.resource.class}}" && attribute == "{{p.resource.attribute}}") 
            {
                //TODO: Implement OCL constraint: {{ p.constraint.ocl }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        public static void PurposeBasedCheckUpdateAttribute(string entity, string attribute, object self, object value)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!PurposeBasedCheckUpdateAttribute(entity, attribute, self, value, caller, actualPurpose)) 
                {
                    throw new PrivacyException("The owner does not give consent to update value of " 
                        + attribute + " of " + entity + " for " + actualPurpose.purpose + " purpose.");
                }
            }
        }

        private static bool PurposeBasedCheckUpdateAttribute(string entity, string attribute, object self, object value, User? caller, Purpose purpose)
        {
            {%- for p in privacy.policy %}
            {%- if p.action == "update" and "class" in p.resource and "attribute" in p.resource %}
            if (entity == "{{p.resource.class}}" && attribute == "{{p.resource.attribute}}") 
            {
                //TODO: Implement OCL constraint: {{ p.constraint.ocl }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
          
        public static void PurposeBasedCheckDeleteObject(string entity, object self)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!PurposeBasedCheckDeleteObject(entity, self, caller, actualPurpose)) 
                {
                    throw new PrivacyException("The owner does not give consent to delete object of type " 
                        + entity + " for " + actualPurpose.purpose + " purpose.");
                }
            }
        }

        private static bool PurposeBasedCheckDeleteObject(string entity, object self, User? caller, Purpose purpose)
        {
            {%- for p in privacy.policy %}
            {%- if p.action == "delete" and "class" in p.resource %}
            if (entity == "{{p.resource.class}}") 
            {
                //TODO: Implement OCL constraint: {{ p.constraint.ocl }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        public static void PurposeBasedCheckAddAssociationEnd(string entity, string end, object self, object target)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!PurposeBasedCheckAddAssociationEnd(entity, end, self, target, caller, actualPurpose)) 
                {
                    throw new PrivacyException("The owner does not give consent to add object to end " + end + " of " 
                        + entity + " for " + actualPurpose.purpose + " purpose.");
                }
            }
        }

        private static bool PurposeBasedCheckAddAssociationEnd(string entity, string end, object self, object target, User? caller, Purpose purpose)
        {
            {%- for p in privacy.policy %}
            {%- if p.action == "create" and "association" in p.resource %}
            if (association == "{{p.resource.association}}") 
            {
                //TODO: Implement OCL constraint: {{ p.constraint.ocl }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        public static void PurposeBasedCheckRemoveAssociationEnd(string entity, string end, object self, object target)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!PurposeBasedCheckRemoveAssociationEnd(entity, end, self, target, caller, actualPurpose)) 
                {
                    throw new PrivacyException("The owner does not give consent to remove object to end " + end + " of " 
                        + entity + " for " + actualPurpose.purpose + " purpose.");
                }
            }
        }

        private static bool PurposeBasedCheckRemoveAssociationEnd(string entity, string end, object self, object target, User? caller, Purpose purpose)
        {
            {%- for p in privacy.policy %}
            {%- if p.action == "delete" and "association" in p.resource %}
            if (association == "{{p.resource.association}}") 
            {
                //TODO: Implement OCL constraint: {{ p.constraint.ocl }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        public static void PurposeBasedCheckReadAssociationEnd(string entity, string end, object self)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!PurposeBasedCheckReadAssociationEnd(entity, end, self, caller, actualPurpose)) 
                {
                    throw new PrivacyException("The owner does not give consent to read objectss of end " + end + " of " 
                        + entity + " for " + actualPurpose.purpose + " purpose.");
                }
            }         
        }

        private static bool PurposeBasedCheckReadAssociationEnd(string entity, string end, object self, User? caller, Purpose purpose)
        {
            {%- for p in privacy.policy %}
            {%- if p.action == "read" and "association" in p.resource %}
            if (association == "{{p.resource.association}}") 
            {
                //TODO: Implement OCL constraint: {{ p.constraint.ocl }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        public static void PurposeBasedCheckExecuteMethod(string entity, string methodName, object self)
        {
            User? caller = Context.GetCurrentUser();
            foreach (Purpose actualPurpose in actualPurposes)
            {
                if (!PurposeBasedCheckExecuteMethod(entity, methodName, self, caller, actualPurpose)) 
                {
                    throw new PrivacyException("The owner does not give consent to execute method " 
                        + methodName + " of class " + entity + " for " + actualPurpose.purpose + " purpose.");
                }
            }  
        }

        private static bool PurposeBasedCheckExecuteMethod(string entity, string methodName, object self, User? caller, Purpose purpose)
        {
            {%- for p in privacy.policy %}
            {%- if p.action == "execute" and "class" in p.resource and "method" in p.resource %}
            if (entity == "{{p.resource.class}}" && methodName == "{{p.resource.method}}") 
            {
                //TODO: Implement OCL constraint: {{ p.constraint.ocl }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
    }
}
