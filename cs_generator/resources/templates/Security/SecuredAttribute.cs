﻿// (c) 2023 Anonymous Author(s)
// Generated code

using PostSharp.Aspects;
using PostSharp.Serialization;
using {{project}}.Privacy;

namespace {{project}}.Security
{
    [PSerializable]
    public class SecuredAttribute : LocationInterceptionAspect
    {
        public string? purpose { get; set; }

        // Code generated for authorization constraints
        public override void OnGetValue(LocationInterceptionArgs args)
        {
            object self = args.Instance;
            string entityName = args.Binding.LocationInfo.DeclaringType.Name; 
            string attributeName = args.Binding.LocationInfo.Name;
            SecurityEnforcer.AuthorizationCheckReadAttribute(entityName, attributeName, self);
            PersonalDataAttribute? personalDataAttribute = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(self.GetType(), typeof(PersonalDataAttribute));
            if (personalDataAttribute != null)
            {
                PurposeTracking.CheckForConsents(self);
                PurposeTracking.PurposeBasedCheckReadAttribute(entityName, attributeName, self);
            }
            base.OnGetValue(args);
        }

        // Code generated for authorization constraints
        public override void OnSetValue(LocationInterceptionArgs args)
        {
            object value = args.Value;
            object self = args.Instance;
            string entityName = args.Binding.LocationInfo.DeclaringType.Name;
            string attributeName = args.Binding.LocationInfo.Name;
            SecurityEnforcer.AuthorizationCheckUpdateAttribute(entityName, attributeName, self, value);
            PersonalDataAttribute? personalDataAttribute = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(self.GetType(), typeof(PersonalDataAttribute));
            if (personalDataAttribute != null)
            {
                PurposeTracking.CheckForConsents(self);
                PurposeTracking.PurposeBasedCheckUpdateAttribute(entityName, attributeName, self, value);
            }
            base.OnSetValue(args);
        }
    }
}
