﻿// (c) 2023 Anonymous Author(s)
// Generated code

using PostSharp.Aspects;
using PostSharp.Serialization;
using {{project}}.Models;
using {{project}}.Privacy;
using {{project}}.Error;

namespace {{project}}.Security
{
    [PSerializable]
    public class SecuredEndMethod : OnMethodBoundaryAspect
    {
        public string? purpose { get; set; }

        public override void OnEntry(MethodExecutionArgs args)
        {
            // Code generated for authorization checks
            string methodName = args.Method.Name;
            Type entity = args.Method.DeclaringType;
            object self = args.Instance;
            if (methodName.StartsWith("Add"))
            {
                object target = args.Arguments.GetArgument(0);
                SecurityEnforcer.AuthorizationCheckAddAssociationEnd(entity.Name, methodName.Substring(3, methodName.Length - 3), self, target);
                PersonalDataAttribute? personalDataAttributeLeft = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(self.GetType(), typeof(PersonalDataAttribute));
                if (personalDataAttributeLeft != null)
                {
                    PurposeTracking.CheckForConsents(self);
                    PurposeTracking.PurposeBasedCheckAddAssociationEnd(entity.Name, methodName.Substring(3, methodName.Length - 3), self, target);
                }
                PersonalDataAttribute? personalDataAttributeRight = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(target.GetType(), typeof(PersonalDataAttribute));
                if (personalDataAttributeRight != null)
                {
                    PurposeTracking.CheckForConsents(target);
                    //PurposeTracking.PurposeBasedCheckAddAssociationEnd(entity.Name, methodName.Substring(3, methodName.Length - 3), self, target);
                }
            }
            else if (methodName.StartsWith("Remove"))
            {
                object target = args.Arguments.GetArgument(0);
                SecurityEnforcer.AuthorizationCheckRemoveAssociationEnd(entity.Name, methodName.Substring(6, methodName.Length - 6), self, target);
                PersonalDataAttribute? personalDataAttributeLeft = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(self.GetType(), typeof(PersonalDataAttribute));
                if (personalDataAttributeLeft != null)
                {
                    PurposeTracking.CheckForConsents(self);
                    PurposeTracking.PurposeBasedCheckRemoveAssociationEnd(entity.Name, methodName.Substring(6, methodName.Length - 6), self, target);
                }
                PersonalDataAttribute? personalDataAttributeRight = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(target.GetType(), typeof(PersonalDataAttribute));
                if (personalDataAttributeRight != null)
                {
                    PurposeTracking.CheckForConsents(target);
                    //PurposeTracking.PurposeBasedCheckRemoveAssociationEnd(entity.Name, methodName.Substring(6, methodName.Length - 6), self, target);
                }
            }
            else
            {
                SecurityEnforcer.AuthorizationCheckReadAssociationEnd(entity.Name, methodName, self);
                PersonalDataAttribute? personalDataAttribute = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(self.GetType(), typeof(PersonalDataAttribute));
                if (personalDataAttribute != null)
                {
                    PurposeTracking.CheckForConsents(self);
                    PurposeTracking.PurposeBasedCheckReadAssociationEnd(entity.Name, methodName, self);
                }
            }

            // Code generated for collecting declared purposes
            if (purpose != null)
            {
                PurposeTracking.push(purpose);
            }
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            // Code generated for removing declared purposes
            if (purpose != null)
            {
                PurposeTracking.pop(purpose);
                if (PurposeTracking.actualPurposes.Count == 0 && PurposeTracking.violatedPolicies.Count > 0)
                {
                    PrivacyException pe = new PrivacyException("The current user does not have the consents to perform the given action on selected resources.");
                    PurposeTracking.violatedPolicies.Clear();
                    throw pe;
                }
            }
        }
    }
}