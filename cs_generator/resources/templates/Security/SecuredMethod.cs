﻿// (c) 2023 Anonymous Author(s)
// Generated code

using PostSharp.Aspects;
using PostSharp.Serialization;
using {{project}}.Models;
using {{project}}.Privacy;

namespace {{project}}.Security
{
    [PSerializable]
    public class SecuredMethod : OnMethodBoundaryAspect
    {
        public string? purpose { get; set; }

        public override void OnEntry(MethodExecutionArgs args)
        {
            // Code generated for authorization checks
            string methodName = args.Method.Name;
            Type entity = args.Method.DeclaringType;
            if (methodName == "Create")
            {
                SecurityEnforcer.AuthorizationCheckCreateObject(entity.Name);
            }
            else if (methodName == "Delete")
            {
                object self = args.Instance;
                SecurityEnforcer.AuthorizationCheckDeleteObject(entity.Name, self);
                PersonalDataAttribute? personalDataAttribute = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(self.GetType(), typeof(PersonalDataAttribute));
                if (personalDataAttribute != null)
                {
                    PurposeTracking.CheckForConsents(self);
                    PurposeTracking.PurposeBasedCheckDeleteObject(entity.Name, self);
                }
            } 
            else
            {
                object self = args.Instance;
                SecurityEnforcer.AuthorizationCheckExecuteMethod(entity.Name, methodName, self);
                PersonalDataAttribute? personalDataAttribute = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(self.GetType(), typeof(PersonalDataAttribute));
                if (personalDataAttribute != null)
                {
                    PurposeTracking.CheckForConsents(self);
                    PurposeTracking.PurposeBasedCheckExecuteMethod(entity.Name, methodName, self);
                }
            }
            
            // Code generated for collecting declared purposes
            if (purpose != null)
            {
                PurposeTracking.push(purpose);
            }
        }

        public override void OnExit(MethodExecutionArgs args)
        {
            string methodName = args.Method.Name;
            Type entity = args.Method.DeclaringType;
            object self = args.Instance;
            if (methodName == "Create")
            {
                PersonalDataAttribute? personalDataAttribute = (PersonalDataAttribute?)System.Attribute.GetCustomAttribute(self.GetType(), typeof(PersonalDataAttribute));
                if (personalDataAttribute != null)
                {
                    PurposeTracking.PurposeBasedCheckCreateObject(entity.Name);
                }
            }

            // Code generated for removing declared purposes
            if (purpose != null)
            {
                PurposeTracking.pop(purpose);
            }
        }
    }
}