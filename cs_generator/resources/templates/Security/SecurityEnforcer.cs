﻿// (c) 2023 Anonymous Author(s)
// Generated code

using {{project}}.Data;
using {{project}}.Models;
using UnauthorizedAccessException = {{project}}.Error.UnauthorizedAccessException;

namespace {{project}}.Security
{
    public class SecurityEnforcer
    {
        // Authorization checks
        public static void AuthorizationCheckCreateObject(string entity)
        {
            User caller = Context.GetCurrentUser();
            Role? role = Context.GetRole(caller);
            bool res = false;
            if (role == null) {
                throw new UnauthorizedAccessException("No access right for create object of class " + entity);
            }
            if (role.Name == "{{ config.Visitor.Name }}")
            {
                res = AuthorizationCheckCreateObjectVisitor(entity, caller);
            }
            {%- for r in security.roles %}
            if (role.Name == "{{ r.name }}")
            {
                res = AuthorizationCheckCreateObject{{ r.name }}(entity, caller);
            }
            {%- endfor %}
            if (!res) 
            {
                throw new UnauthorizedAccessException("No access right for create object of class " + entity);
            }
        }

        private static bool AuthorizationCheckCreateObjectVisitor(string entity, User? caller)
        {
            {%- for p in security.policy %}
            {%- if "Visitor" == p.role and p.action == "create" and "class" in p.resource %}
            if (entity == "{{ p.resource.class }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        {%- for r in security.roles %}
        private static bool AuthorizationCheckCreateObject{{ r.name }}(string entity, User? caller)
        {
            {%- if "extends" in r %}
            {%- for s in r.extends %}
            if (AuthorizationCheckCreateObject{{ s }}(entity, caller)) 
            { 
                return true;
            }
            {%- endfor %}
            {%- endif %}
            {%- for p in security.policy %}
            {%- if r.name == p.role and p.action == "create" and "class" in p.resource %}
            if (entity == "{{ p.resource.class }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
        {%- endfor %}
            
        public static void AuthorizationCheckReadAttribute(string entity, string attribute, object self)
        {
            User caller = Context.GetCurrentUser();
            Role? role = Context.GetRole(caller);
            bool res = false;
            if (role == null) {
                throw new UnauthorizedAccessException("No access right for read attribute " + attribute + " of class " + entity);
            }
            if (role.Name == "{{ config.Visitor.Name }}")
            {
                res = AuthorizationCheckReadAttributeVisitor(entity, attribute, self, caller);
            }
            {%- for r in security.roles %}
            if (role.Name == "{{ r.name }}")
            {
                res = AuthorizationCheckReadAttribute{{ r.name }}(entity, attribute, self, caller);
            }
            {%- endfor %}
            if (!res)
            {
                throw new UnauthorizedAccessException("No access right for read attribute " + attribute + " of class " + entity);
            }
        }

        private static bool AuthorizationCheckReadAttributeVisitor(string entity, string attribute, object self, User? caller)
        {
            {%- for p in security.policy %}
            {%- if "Visitor" == p.role and p.action == "read" and "attribute" in p.resource %}
            if (entity == "{{ p.resource.class }}" && attribute == "{{ p.resource.attribute }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        {%- for r in security.roles %}
        private static bool AuthorizationCheckReadAttribute{{ r.name }}(string entity, string attribute, object self, User? caller)
        {
            {%- if "extends" in r %}
            {%- for s in r.extends %}
            if (AuthorizationCheckReadAttribute{{ s }}(entity, attribute, self, caller))
            { 
                return true;
            }
            {%- endfor %}
            {%- endif %}
            {%- for p in security.policy %}
            {%- if r.name == p.role and p.action == "read" and "attribute" in p.resource %}
            if (entity == "{{ p.resource.class }}" && attribute == "{{ p.resource.attribute }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
        {%- endfor %}

        public static void AuthorizationCheckUpdateAttribute(string entity, string attribute, object self, object value)
        {
            User caller = Context.GetCurrentUser();
            Role? role = Context.GetRole(caller);
            bool res = false;
            if (role == null) {
                throw new UnauthorizedAccessException("No access right for update attribute " + attribute + " of class " + entity + " with value " + value.ToString());
            }
            if (role.Name == "{{ config.Visitor.Name }}")
            {
                res = AuthorizationCheckUpdateAttributeVisitor(entity, attribute, self ,value, caller);
            }
            {%- for r in security.roles %}
            if (role.Name == "{{ r.name }}")
            {
                res = AuthorizationCheckUpdateAttribute{{ r.name }}(entity, attribute, self ,value, caller);
            }
            {%- endfor %}
            if (!res)
            {
                throw new UnauthorizedAccessException("No access right for update attribute " + attribute + " of class " + entity + " with value " + value.ToString());
            }
        }

        private static bool AuthorizationCheckUpdateAttributeVisitor(string entity, string attribute, object value, object self, User? caller)
        {
            {%- for p in security.policy %}
            {%- if "Visitor" == p.role and p.action == "update" and "attribute" in p.resource %}
            if (entity == "{{ p.resource.class }}" && attribute == "{{ p.resource.attribute }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        {%- for r in security.roles %}
        private static bool AuthorizationCheckUpdateAttribute{{ r.name }}(string entity, string attribute, object value, object self, User? caller)
        {
            {%- if "extends" in r %}
            {%- for s in r.extends %}
            if (AuthorizationCheckUpdateAttribute{{ s }}(entity, attribute, value, self, caller)) 
            { 
                return true;
            }
            {%- endfor %}
            {%- endif %}
            {%- for p in security.policy %}
            {%- if r.name == p.role and p.action == "update" and "attribute" in p.resource %}
            if (entity == "{{ p.resource.class }}" && attribute == "{{ p.resource.attribute }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
        {%- endfor %}
    
        public static void AuthorizationCheckDeleteObject(string entity, object self)
        {
            User caller = Context.GetCurrentUser();
            Role? role = Context.GetRole(caller);
            bool res = false;
            if (role == null) {
                throw new UnauthorizedAccessException("No access right for delete object of class " + entity);
            }
            if (role.Name == "{{ config.Visitor.Name }}")
            {
                res = AuthorizationCheckDeleteObjectVisitor(entity, self, caller);
            }
            {%- for r in security.roles %}
            if (role.Name == "{{ r.name }}")
            {
                res = AuthorizationCheckDeleteObject{{ r.name }}(entity, self, caller);
            }
            {%- endfor %}
            if (!res)
            {
                throw new UnauthorizedAccessException("No access right for delete object of class " + entity);
            }
        }

        private static bool AuthorizationCheckDeleteObjectVisitor(string entity, object self, User? caller)
        {
            {%- for p in security.policy %}
            {%- if "Visitor" == p.role and p.action == "delete" and "class" in p.resource %}
            if (entity == "{{ p.resource.class }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        {%- for r in security.roles %}
        private static bool AuthorizationCheckDeleteObject{{ r.name }}(string entity, object self, User? caller)
        {
            {%- if "extends" in r %}
            {%- for s in r.extends %}
            if (AuthorizationCheckDeleteObject{{ s }}(entity, self, caller)) 
            { 
                return true;
            }
            {%- endfor %}
            {%- endif %}
            {%- for p in security.policy %}
            {%- if r.name == p.role and p.action == "delete" and "class" in p.resource %}
            if (entity == "{{ p.resource.class }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
        {%- endfor %}
        
        public static void AuthorizationCheckAddAssociationEnd(string entity, string end, object self, object target)
        {
            User caller = Context.GetCurrentUser();
            Role? role = Context.GetRole(caller);
            bool res = false;
            if (role == null) {
                throw new UnauthorizedAccessException("No access right for add new object to association end " + end + " of class " + entity);
            }
            if (role.Name == "{{ config.Visitor.Name }}")
            {
                res = AuthorizationCheckAddAssociationEndVisitor(entity, end, self, target, caller);
            }
            {%- for r in security.roles %}
            if (role.Name == "{{ r.name }}")
            {
                res = AuthorizationCheckAddAssociationEnd{{ r.name }}(entity, end, self, target, caller);
            }
            {%- endfor %}
            if (!res)
            {
                throw new UnauthorizedAccessException("No access right for add new object to association end " + end + " of class " + entity);
            }
        }

        private static bool AuthorizationCheckAddAssociationEndVisitor(string entity, string end, object self, object target, User? caller)
        {
            {%- for p in security.policy %}
            {%- if "Visitor" == p.role and p.action == "add" and "ends" in p.resource %}
            if (entity == "{{ p.resource.class }}" && end == "{{ p.resource.ends }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        {%- for r in security.roles %}
        private static bool AuthorizationCheckAddAssociationEnd{{ r.name }}(string entity, string end, object self, object target, User? caller)
        {
            {%- if "extends" in r %}
            {%- for s in r.extends %}
            if (AuthorizationCheckAddAssociationEnd{{ s }}(entity, end, self, target, caller)) 
            { 
                return true;
            }
            {%- endfor %}
            {%- endif %}
            {%- for p in security.policy %}
            {%- if r.name == p.role and p.action == "add" and "ends" in p.resource %}
            if (entity == "{{ p.resource.class }}" && end == "{{ p.resource.ends }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
        {%- endfor %}

        public static void AuthorizationCheckRemoveAssociationEnd(string entity, string end, object self, object target)
{
            User caller = Context.GetCurrentUser();
            Role? role = Context.GetRole(caller);
            bool res = false;
            if (role == null) {
                throw new UnauthorizedAccessException("No access right for remove object of association end " + end + " of class " + entity);
            }
            if (role.Name == "{{ config.Visitor.Name }}")
            {
                res = AuthorizationCheckRemoveAssociationEndVisitor(entity, end, self, target, caller);
            }
            {%- for r in security.roles %}
            if (role.Name == "{{ r.name }}")
            {
                res = AuthorizationCheckRemoveAssociationEnd{{ r.name }}(entity, end, self, target, caller);
            }
            {%- endfor %}
            if (!res)
            {
                throw new UnauthorizedAccessException("No access right for remove object of association end " + end + " of class " + entity);
            }
        }

        private static bool AuthorizationCheckRemoveAssociationEndVisitor(string entity, string end, object self, object target, User? caller)
        {
            {%- for p in security.policy %}
            {%- if "Visitor" == p.role and p.action == "remove" and "ends" in p.resource %}
            if (entity == "{{ p.resource.class }}" && end == "{{ p.resource.ends }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        {%- for r in security.roles %}
        private static bool AuthorizationCheckRemoveAssociationEnd{{ r.name }}(string entity, string end, object self, object target, User? caller)
        {
            {%- if "extends" in r %}
            {%- for s in r.extends %}
            if (AuthorizationCheckRemoveAssociationEnd{{ s }}(entity, end, self, target, caller)) 
            { 
                return true;
            }
            {%- endfor %}
            {%- endif %}
            {%- for p in security.policy %}
            {%- if r.name == p.role and p.action == "remove" and "ends" in p.resource %}
            if (entity == "{{ p.resource.class }}" && end == "{{ p.resource.ends }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
        {%- endfor %}

        public static void AuthorizationCheckReadAssociationEnd(string entity, string end, object self)
{
            User caller = Context.GetCurrentUser();
            Role? role = Context.GetRole(caller);
            bool res = false;
            if (role == null) {
                throw new UnauthorizedAccessException("No access right for read association end " + end + " of class " + entity);
            }
            if (role.Name == "{{ config.Visitor.Name }}")
            {
                res = AuthorizationCheckReadAssociationEndVisitor(entity, end, self, caller);
            }
            {%- for r in security.roles %}
            if (role.Name == "{{ r.name }}")
            {
                res = AuthorizationCheckReadAssociationEnd{{ r.name }}(entity, end, self, caller);
            }
            {%- endfor %}
            if (!res)
            {
                throw new UnauthorizedAccessException("No access right for read association end " + end + " of class " + entity);
            }
        }

        private static bool AuthorizationCheckReadAssociationEndVisitor(string entity, string end, object self, User? caller)
        {
            {%- for p in security.policy %}
            {%- if "Visitor" == p.role and p.action == "read" and "ends" in p.resource %}
            if (entity == "{{ p.resource.class }}" && end == "{{ p.resource.ends }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        {%- for r in security.roles %}
        private static bool AuthorizationCheckReadAssociationEnd{{ r.name }}(string entity, string end, object self, User? caller)
        {
            {%- if "extends" in r %}
            {%- for s in r.extends %}
            if (AuthorizationCheckReadAssociationEnd{{ s }}(entity, end, self, caller)) 
            { 
                return true;
            }
            {%- endfor %}
            {%- endif %}
            {%- for p in security.policy %}
            {%- if r.name == p.role and p.action == "read" and "ends" in p.resource %}
            if (entity == "{{ p.resource.class }}" && end == "{{ p.resource.ends }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
        {%- endfor %}

        public static void AuthorizationCheckExecuteMethod(string entity, string methodName, object self)
        {
            User caller = Context.GetCurrentUser();
            Role? role = Context.GetRole(caller);
            bool res = false;
            if (role == null) {
                throw new UnauthorizedAccessException("No access right for execute method " + methodName + " of " + entity);
            }
            if (role.Name == "{{ config.Visitor.Name }}")
            {
                res = AuthorizationCheckExecuteMethodVisitor(entity, methodName, self, caller);
            }
            {%- for r in security.roles %}
            if (role.Name == "{{ r.name }}")
            {
                res = AuthorizationCheckExecuteMethod{{ r.name }}(entity, methodName, self, caller);
            }
            {%- endfor %}
            if (!res)
            {
                throw new UnauthorizedAccessException("No access right for execute method " + methodName + " of " + entity);
            }
        }

        private static bool AuthorizationCheckExecuteMethodVisitor(string entity, string methodName, object self, User? caller)
        {
            {%- for p in security.policy %}
            {%- if "Visitor" == p.role and p.action == "execute" and "class" in p.resource %}
            if (entity == "{{ p.resource.class }}" && methodName == "{{ p.resource.method }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }

        {%- for r in security.roles %}
        private static bool AuthorizationCheckExecuteMethod{{ r.name }}(string entity, string methodName, object self, User? caller)
        {
            {%- if "extends" in r %}
            {%- for s in r.extends %}
            if (AuthorizationCheckExecuteMethod{{ s }}(entity, methodName, self, caller)) 
            { 
                return true;
            }
            {%- endfor %}
            {%- endif %}
            {%- for p in security.policy %}
            {%- if r.name == p.role and p.action == "execute" and "class" in p.resource %}
            if (entity == "{{ p.resource.class }}" && methodName == "{{ p.resource.method }}")
            {
                //TODO: Implement OCL constraint: {{ p.auth }}
                return true;
            }
            {%- endif %}
            {%- endfor %}
            return false;
        }
        {%- endfor %}
    }
}
