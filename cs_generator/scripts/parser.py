#!/usr/bin/env python3
"""
@author: Anonymous Author(s)
"""

import os
import shutil
import json
from os.path import isfile
from jinja2 import Environment, FileSystemLoader, select_autoescape

"""
Simple C# generator for privacy enhanced web applications
"""
BASE_DIRECTORY = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
PROJECT = "default"
CONFIG = None
DATA = None
SECURITY = None
PRIVACY = None

"""
JSON parser
"""
class JSONObject(object):
    def __init__(self, d):
        self.__dict__ = d

"""
Directory navigation
"""
def clean_dir(*path):
    dir = os.path.join(BASE_DIRECTORY, *path)
    if os.path.exists(dir):
        shutil.rmtree(dir)
    os.mkdir(dir)

def set_working_directory(*path):
    dir = os.path.join(BASE_DIRECTORY, *path)
    os.chdir(dir)

"""
Auxiliary functions
"""

primitives = {
    "String":'string',
    "Boolean":'bool',
    "Integer":'long'
}

def is_primitive(t):
    return t in primitives

def type(t):
    return primitives[t]

def is_personalData(c,privacy):
    """
    return True if c is a personalData w.r.t. model pm
    """
    for data in privacy['personalData']:
        if c == data:
            return True
    return False

def params(params):
    s = ''
    if len(params) == 1:
        return '{0} {1}'.format(params[0]['type'], params[0]['name'])
    first = True
    for param in params:
        if first:
            first = False
        else:
            s += ', '
        s += '{0} {1}'.format(param['type'], param['name'])
    return s

def label(c,method,privacy):
    """
    label actual purposes to method met of class c w.r.t. model pm
    """
    # TODO: Extend with the multiple purposes
    for p in privacy['purposes']:
        for end in p['endpoints']:
            if end['class'] == c['class'] and end['met'] == method:
                return f"(purpose=\"p['name']\")"
    return ""

def init_environment():
    return Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )

def generate_file(source,target):
    print("Generate file {0} -> {1}".format(source, target))
    env = init_environment()
    source_env = env.get_template(source)
    source_env.globals['type'] = type
    source_env.globals['is_primitive'] = is_primitive
    source_env.globals['params'] = params
    source_env.globals['is_personalData'] = is_personalData
    render = source_env.render(project=PROJECT,config=CONFIG,data=DATA,security=SECURITY,privacy=PRIVACY)
    with open(target,"w+",encoding="utf-8") as f:
        f.write(render)

def generate_files(*path):
    set_working_directory("resources", "templates", *path)
    sources = os.listdir()
    for source in sources:
        if isfile(source):
            target = os.path.join(BASE_DIRECTORY, "output", PROJECT, *path, source)
            generate_file(source,target)

### Main generator

def generate_EM():
    clean_dir("output", PROJECT, "Security")
    generate_files("Security")
    generate_files("Views", "Shared")
    clean_dir("output", PROJECT, "Privacy")
    generate_files("Privacy")

def generate_ApplicationContext():
    generate_files()

def generate_Consent_Collection():
    generate_files("Controllers")
    clean_dir("output", PROJECT, "Views", "Privacy")
    generate_files("Views", "Privacy")
    clean_dir("output", PROJECT, "Views", "Home")
    generate_files("Views", "Home")

def generate_project():
    generate_EFModels()
    generate_EM()
    generate_Consent_Collection()
    generate_Others()

def generate_ErrorHandler():
    clean_dir("output", PROJECT, "Error")
    generate_files("Error")

def generate_Others():
    generate_ApplicationContext()
    generate_ErrorHandler()

def generate_EFClass(c):
    set_working_directory("resources", "templates", "Models", "Data")
    source = "EFClass.cs"
    target = os.path.join(BASE_DIRECTORY, "output", PROJECT, "Models", f"{c['class']}.cs")
    env = init_environment()
    source_env = env.get_template(source)
    source_env.globals['type'] = type
    source_env.globals['is_primitive'] = is_primitive
    source_env.globals['params'] = params
    source_env.globals['is_personalData'] = is_personalData
    render = source_env.render(c=c,project=PROJECT,config=CONFIG,data=DATA,security=SECURITY,privacy=PRIVACY)
    with open(target,"w+",encoding="utf-8") as f:
        f.write(render)

def generate_ApplicationDbContext():
    clean_dir("output", PROJECT, "Data")
    generate_files("Data")

def generate_EFModels():
    """
    Generate EF classes correspond to data model
    """
    generate_ApplicationDbContext()
    for c in DATA:
        if 'isAssociation' not in c or not c['isAssociation']:
            generate_EFClass(c)   
    generate_files("Models")

def transform_security():
    # Associate data class with declared user class in the security model (if any)
    if 'userClass' in SECURITY:
        for c in DATA:
            if c['class'] == SECURITY['userClass']:
                if 'attributes' not in c:
                    c['attributes'] = [{'name': 'user', 'type': 'User'}]
                else:
                    c['attributes'].append({'name': 'user', 'type': 'User'})

def transform_privacy():
    # Associate data classes with declared personal data class in privacy model (if any)
    # Adding Any{} purpose into the purpose hierarchy
    for name in PRIVACY['personalData']:
        for c in DATA:
            if c['class'] == name:
                if 'attributes' not in c:
                    c['attributes'] = [{'name': 'data', 'type': 'PersonalData'}]
                else:
                    c['attributes'].append({'name': 'data', 'type': 'PersonalData'})
    any_purpose = {'name': 'Any', 'endpoints': [], 'includes': []}
    for purpose in PRIVACY['purposes']:
        for other_purpose in PRIVACY['purposes']:
            if purpose != other_purpose and purpose['name'] not in other_purpose['includes']:
                any_purpose['includes'].append(purpose['name'])
    PRIVACY['purposes'].append(any_purpose)

def transform_data():
    # Put association ends into classes
    for c in DATA:
        if 'isAssociation' in c and c['isAssociation']:
            # We are currently support binary association
            left_end = c['ends'][0]
            right_end = c['ends'][1]
            for c_ in DATA:
                if c_['class'] == left_end['target']:
                    if 'ends' not in c_:
                        c_['ends'] = [right_end]
                    else:
                        c_['ends'].append(right_end)
                if c_['class'] == right_end['target']:
                    if 'ends' not in c_:
                        c_['ends'] = [left_end]
                    else:
                        c_['ends'].append(left_end)

def transform_models():
    """
    This function transform a data model into a new data model 
    that includes additional information related to authentication and privacy
    """
    transform_data()
    transform_security()
    transform_privacy()

if __name__ == "__main__":
    
    print("Running parser with root directory " + BASE_DIRECTORY)

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-g", "--generate",
                        help="generate code",
                        action="store_true")

    parser.add_argument('-p','--projectname', 
                        help='project name', 
                        required=True)              
    args = parser.parse_args()

    PROJECT = args.projectname

    set_working_directory("config")
    with open("config.json", "r") as config_file:
        CONFIG = json.load(config_file, object_hook=JSONObject)

    set_working_directory("models", PROJECT)
    with open("dm.json", "r") as f:
        DATA = json.load(f)
    with open("sm.json", "r") as f:
        SECURITY = json.load(f)
    with open("pm.json", "r") as f:
        PRIVACY = json.load(f)

    no_args = all(not val for val in vars(args).values())

    if args.generate:
        transform_models()
        generate_project()
