#!/bin/bash

# PARAMETERS

PROJECTNAME="${1}"	# Parameter for indicating the project name
echo "project name = $PROJECTNAME"

# SETUP

# Step 1: Initialize project
echo "# Step 1: Initialize project"

cd ../output				# Navigate to the output folder, this is where all of the generated code saved
rm -rf ${PROJECTNAME}			# Remove the old folder with the same project name, if any
mkdir ${PROJECTNAME}			# Making a new directory with the project name.
cd ${PROJECTNAME}			# Switch to the newly create directory as current directory

# Step 2: Build an MVC .NET project
echo "# Step 2: Build an MVC .NET project"

dotnet new mvc -f net7.0 --force

# Step 3: Install packages
echo "# Step 3: Install related packages"

dotnet new tool-manifest

dotnet tool install dotnet-aspnet-codegenerator --version 7.0.9 # Scaffolding feature in .NET to speed up development.
dotnet add package Microsoft.AspNetCore.Diagnostics.EntityFrameworkCore --version 7.0.10 # EFCore packages
dotnet add package Microsoft.AspNetCore.Identity.EntityFrameworkCore --version 7.0.10 # EFCore packages
dotnet add package Microsoft.AspNetCore.Identity.UI --version 7.0.10 # EFCore packages

dotnet tool install dotnet-ef --version 7.0.10 # Entity Framework
dotnet add package Microsoft.EntityFrameworkCore.SqlServer --version 7.0.10 # EFCore packages: SQLServer
dotnet add package Microsoft.EntityFrameworkCore.Tools --version 7.0.10 # EFCore packages
dotnet add package Microsoft.VisualStudio.Web.CodeGeneration.Design --version 7.0.9  # Auxiliary package

dotnet add package PostSharp --version 2023.0.7 # PostSharp for Aspect-oriented programming

# Step 4: Generate artifacts
echo "# Step 4: Generate artifacts"

cd ../../scripts
python3 parser.py -p ${PROJECTNAME} -g

# Step 5: Create database
echo "# Step 5: Generate database"

cd ../output/${PROJECTNAME}

dotnet ef migrations add "$(date +%F_%H-%M-%S)"
dotnet ef database update

# Step 6: Scaffolding Identity services (for Login and Register)
echo "# Step 6: Scaffolding Identity services (for Login and Register)"
dotnet aspnet-codegenerator identity -dc ${PROJECTNAME}.Data.ApplicationDbContext
cp -fr ../../resources/templates/Views/Shared/_LoginPartial.cshtml Views/Shared
mv Register.cshtml.cs Areas/Identity/Pages/Account

# Step 7: Build project
echo "# Step 7: Build project"
dotnet build
