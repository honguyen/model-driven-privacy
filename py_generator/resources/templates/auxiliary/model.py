from enum import Enum, auto


# Fixed set of actions
class Action(Enum):
    read = auto()
    update = auto()
    add = auto()
    remove = auto()
    create = auto()
    delete = auto()

# Pre-defined set of constraints
class Constraint:
    fullAccess   = lambda *x, **y: True
    noAccess     = lambda *x, **y: False
    createAccess = lambda caller = None: True
    deleteAccess = lambda caller = None: True
    readAccess   = lambda caller = None, self = None:  True
    updateAccess = lambda caller = None, self = None,  value  = None: True
    addAccess    = lambda caller = None, self = None,  target = None: True
    removeAccess = lambda caller = None, self = None,  target = None: True
