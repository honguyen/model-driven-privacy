# Copyright (c) 2023 All Rights Reserved
# Generated code

from application import app
from flask import render_template, redirect, url_for
from flask_user import UserManager, user_registered, login_required, current_user
from dtm import db, {{sm.userClass}}, {% for r in sm.roles -%} {{r.name}}, {% endfor %} {%- for p in pm.purposes -%} {{p.name}}, {% endfor %} Role, Purpose, Consent, PersonalData
from instrumentation import SecurityException, secure
from ptm import {{projectname}}PrivacyModel


@user_registered.connect_via(app)
def after_user_registered_hook(sender, user, **extra):
    role = Role.query.filter_by(name={{- sm.defaultRole}}).one()
    user.role=role
    db.session.commit()

db.init_app(app)


with app.app_context():
    db.create_all()

    roles = Role.query.all()
    if len(roles) == 0:
        {% for r in sm.roles -%} 
        db.session.add(Role(name={{- r.name}}))
        {% endfor -%}
        db.session.commit()

    purposes = Purpose.query.all()
    if len(purposes) == 0:
        {% for p in pm.purposes -%} 
        db.session.add(Purpose(name={{- p.name}}))
        {% endfor -%}
        db.session.commit()
        {% for p in pm.purposes -%} 
        p = Purpose.query.filter_by(name={{- p.name}}).first()
        {% for sp in p.includes -%} 
            p.subpurposes.append(Purpose.query.filter_by(name={{- sp}}).first())
        {% endfor -%}
        db.session.commit()
        {% endfor %}
    personaldata = PersonalData.query.all()
    if len(personaldata) == 0:
        {% for p in pm.personalData -%} 
        db.session.add(PersonalData(name="{{- p}}"))
        {% endfor -%}
        db.session.commit()

    def P(ls):
        def lazy():
            return list(map(lambda x: Purpose.query.filter_by(name=x).first(),ls))
        return lazy

user_manager = UserManager(app, db, {{- sm.userClass}})

# ENDPOINTS

@app.get('/policy')
@login_required
@secure(db,P([]))
def policy():
    consents = Consent.query.filter_by(user_id=current_user.id).all()
    model = {{projectname}}PrivacyModel.model
    newmodel = []
    tmpmodel = {}
    for p,a,rs,_,d in model:
        purpose = Purpose.query.filter_by(name=p.name).first()
        for r in rs:
            data = PersonalData.query.filter_by(name=r["class"]).first()
            c = list(filter((lambda x: purpose in x.purposes and x.data == data),consents))
            a = r.get("attribute",r.get("ends", None))
            r = r["class"]
            c = c[0] if len(c) > 0 else None
            if  tmpmodel.get((p,r,d,c),None) != None and a != None:
                tmpmodel[(p,r,d,c)].append(a)
            else:
                tmpmodel[(p,r,d,c)] = [a] if a != None else []

    for k in tmpmodel.keys():
        (p,r,d,c) = k
        a = tmpmodel[k]
        newmodel.append((p,a,r,d,c))

    return render_template('privacy.html', consents=consents, model=newmodel)
    
@app.get('/add_consent/<string:pd>/<string:p>')
@login_required
# @secure(db,P([]))
def add_consent(pd,p):
    pd = PersonalData.query.filter_by(name=pd).first()
    p = Purpose.query.filter_by(name=p).first()
    c = Consent.query.filter_by(user_id=current_user.id,data=pd).first()
    if c is not None and p in c.purposes:
        return redirect(url_for('policy'))
    c = Consent(data=pd, user=current_user)
    c.purposes.append(p)
    db.session.add(c)
    db.session.commit()
    return redirect(url_for('policy'))

@app.get('/remove_consent/<int:id>')
@login_required
# @secure(db,P([]))
def remove_consent(id):
    c = Consent.query.get(id)
    if c is not None:
        db.session.delete(c)
        db.session.commit()
    return redirect(url_for('policy'))

@app.route('/error')
def error():
    msg = "You are not allowed to access this page: Not a User"
    return render_template('error.html', message = msg)

@app.route('/')
@secure(db,P([]))
def main():
    # TODO: re-implement the method stub
    return render_template("main.html",user=current_user,security_violation=True,msg="Implement the method stubs in app.py")
{% for c in dm if not c.isAssociation %}
{%- for m in c.methods %}
@app.route('/{{- m.name}}')
@login_required
@secure(db,P({{- get_purpose(m.name)}}))
def {{m.name}}():
    # TODO: implement the method stub
    return "TODO: Implement the method stub" 
{% endfor %}
{%- endfor %}
