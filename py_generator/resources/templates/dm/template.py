# Copyright (c) 2023 All Rights Reserved
# Generated code

from application import app
from flask_sqlalchemy import SQLAlchemy
from flask_user import UserMixin, current_user
from instrumentation import Secure
from stm import {{projectname}}
from ptm import {{projectname}}PrivacyModel


db = SQLAlchemy()

# ROLES
# VISITOR = "VISITOR" # corresponds to current_user.is_authenticated == False
{% for role in sm.roles -%}
{{role.name}} = "{{role.name}}"
{% endfor %}# NONE and SYSTEM roles are not needed explicitly

# PURPOSES
{% for p in pm.purposes -%}
{{p.name}} = "{{p.name}}"
{% endfor %}


with app.app_context():

    # Associations
    consentedpurposes = db.Table('consentedpurposes',
            db.Column('consent_id', db.Integer, db.ForeignKey('consent.id'), nullable=False, primary_key=True),
            db.Column('purpose_id', db.Integer, db.ForeignKey('purpose.id'), nullable=False, primary_key=True)
    )
    {% for c in dm if c.isAssociation-%}
    {% if c.ends[0].target == c.ends[1].target %}
    class {{c.class | lower}}(db.Model):
        
        id = db.Column(db.Integer, primary_key=True)
        {{c.ends[0].name}}_id = db.Column(db.Integer(), db.ForeignKey('{{- c.ends[0].target | lower}}.id'), unique=True)
        {{c.ends[1].name}}_id = db.Column(db.Integer(), db.ForeignKey('{{- c.ends[1].target | lower}}.id'), unique=True)
    
    {% else %}
    class {{c.class | lower}}(db.Model):
            
        id = db.Column(db.Integer, primary_key=True)
        {{c.ends[0].name}}_id = db.Column(db.Integer(), db.ForeignKey('{{- c.ends[0].target | lower}}.id'), unique=True)
        {{c.ends[1].name}}_id = db.Column(db.Integer(), db.ForeignKey('{{- c.ends[1].target | lower}}.id'), unique=True)
        {{c.ends[0].name}} = db.relationship('{{- c.ends[0].target}}', back_populates='{{c.class | lower}}', uselist=False)
        {{c.ends[1].name}} = db.relationship('{{- c.ends[1].target}}', back_populates='{{c.class | lower}}', uselist=False)
    {% endif %}
    {% endfor %}

    # ENTITIES
    {% for c in dm -%}
    {%- if isUserClass(c,sm) %}
    # User class
    @Secure({{projectname}},{{projectname}}PrivacyModel)
    class {{c.class}}(db.Model,UserMixin):

        #Attributes
        id = db.Column(db.Integer, primary_key=True)
        active = db.Column('is_active', db.Boolean(), nullable=False, server_default='1')
        roles = db.relationship('Role', secondary='user_roles')
        username = db.Column(db.String(100, collation='NOCASE'), nullable=False, unique=True)
        password = db.Column(db.String(255), nullable=False, server_default='')
        {% for attr in c.attributes -%}
        {{attr.name}} = db.Column({{type(attr.type)}})
        {% endfor %}
        # Association ends
        {% for ac in dm if ac.isAssociation -%}
        {% if ac.ends[0].target == ac.ends[1].target %}
        {%- if ac.ends[0].target == c.class -%}
        {{ac.ends[1].name}} = db.relationship('{{- ac.ends[1].target}}',
                                 secondary='{{- ac.class | lower}}',
                                 primaryjoin=id=={{- ac.class | lower}}.{{- ac.ends[0].name | lower}}_id,
                                 secondaryjoin=id=={{- ac.class | lower}}.{{- ac.ends[1].name | lower}}_id,
                                 back_populates='{{- ac.ends[0].name}}')

        {% endif -%}  
        {%- if ac.ends[1].target == c.class -%}
        {{ac.ends[0].name}} = db.relationship('{{- ac.ends[0].target}}',
                                 secondary='{{- ac.class | lower}}',
                                 primaryjoin=id=={{- ac.class | lower}}.{{- ac.ends[1].name | lower}}_id,
                                 secondaryjoin=id=={{- ac.class | lower}}.{{- ac.ends[0].name | lower}}_id,
                                 back_populates='{{- ac.ends[1].name}}')
        {% endif -%}    
        {% else %}
        {%- if ac.ends[0].target == c.class -%}
        {{ac.class | lower}} = db.relationship('{{ac.class | lower}}', back_populates='{{ac.ends[0].name}}')
        @property
        def {{ac.ends[1].name}}(self):
            return [x.{{ac.ends[1].name}} for x in self.{{ac.class | lower}}]
        {% endif -%}  
        {%- if ac.ends[1].target == c.class -%}
        {{ac.class | lower}} = db.relationship('{{ac.class | lower}}', back_populates='{{ac.ends[1].name}}')
        @property
        def {{ac.ends[0].name}}(self):
            return [x.{{ac.ends[0].name}} for x in self.{{ac.class | lower}}]
        {% endif -%}    
        {% endif -%}    
        {% endfor %}
        {%- if isPersonalData(c,pm) -%}
        @property
        def owner(self):
            return self
        {% endif -%}
  
        @property
        def role(self):
            return self.roles[0]
        
        @role.setter
        def role(self,r):
            self.roles = [r]
        
        @property
        def is_authenticated(self):
            return True
        
        def __eq__(self, obj):
            return isinstance(obj,UserMixin) and self.id == obj.id

    class Role(db.Model):
        id = db.Column(db.Integer(), primary_key=True)
        name = db.Column(db.String(50), unique=True)

    class UserRoles(db.Model):
        id = db.Column(db.Integer(), primary_key=True)
        user_id = db.Column(db.Integer(), db.ForeignKey('{{- c.class | lower}}.id'), unique=True)
        role_id = db.Column(db.Integer(), db.ForeignKey('role.id'))
    {% else %}
    {%- if not c.isAssociation %}
    @Secure({{projectname}},{{projectname}}PrivacyModel)
    class {{c.class}}(db.Model):

        # Attributes
        id = db.Column(db.Integer, primary_key=True)
        {% for attr in c.attributes -%}
        {{attr.name}} = db.Column({{type(attr.type)}})
        {% endfor %}
        # Association ends
        {% for ac in dm if ac.isAssociation -%}
        {% if ac.ends[0].target == ac.ends[1].target %}
        {%- if ac.ends[0].target == c.class -%}
        {{ac.ends[1].name}} = db.relationship('{{- ac.ends[1].target}}',
                                 secondary='{{- ac.class | lower}}',
                                 primaryjoin=id=={{- ac.class | lower}}.{{- ac.ends[0].name | lower}}_id,
                                 secondaryjoin=id=={{- ac.class | lower}}.{{- ac.ends[1].name | lower}}_id,
                                 back_populates='{{- ac.ends[0].name}}')

        {% endif -%}  
        {%- if ac.ends[1].target == c.class -%}
        {{ac.ends[0].name}} = db.relationship('{{- ac.ends[0].target}}',
                                 secondary='{{- ac.class | lower}}',
                                 primaryjoin=id=={{- ac.class | lower}}.{{- ac.ends[1].name | lower}}_id,
                                 secondaryjoin=id=={{- ac.class | lower}}.{{- ac.ends[0].name | lower}}_id,
                                 back_populates='{{- ac.ends[1].name}}')
        {% endif -%}    
        {% else %}
        {%- if ac.ends[0].target == c.class -%}
        {{ac.class | lower}} = db.relationship('{{ac.class | lower}}', back_populates='{{ac.ends[0].name}}')
        @property
        def {{ac.ends[1].name}}(self):
            return [x.{{ac.ends[1].name}} for x in self.{{ac.class | lower}}]
        {% endif -%}  
        {%- if ac.ends[1].target == c.class -%}
        {{ac.class | lower}} = db.relationship('{{ac.class | lower}}', back_populates='{{ac.ends[1].name}}')
        @property
        def {{ac.ends[0].name}}(self):
            return [x.{{ac.ends[0].name}} for x in self.{{ac.class | lower}}]
        {% endif -%}    
        {% endif -%}    
        {% endfor %}
        {%- if isPersonalData(c,pm) -%}
        owner_id = db.Column(db.Integer, db.ForeignKey('{{- sm.userClass | lower}}.id'), nullable=False)
        owner = db.relationship('{{- sm.userClass}}', foreign_keys=owner_id, backref='personal')
        def __init__(self, **kwargs):
            super({{c.class}}, self).__init__(**kwargs)
            self.owner=current_user
            db.session.commit()
        {% endif -%} 
    {% endif %}
    {% endif %}
    {% endfor %}
    
    class Purpose(db.Model):
        id = db.Column(db.Integer(), primary_key=True)
        name = db.Column(db.String(50), unique=True)
        parent_id = db.Column(db.Integer, db.ForeignKey('purpose.id'))
        subpurposes = db.relationship('Purpose')

        def get_subpurposes(self):
            if self.subpurposes == []:
                return self.name
            else:
                ret = []
                for sp in self.subpurposes:
                    ret += sp.all()
                return ret

        # consents = db.relationship('Consent', secondary=consentedpurposes, lazy='subquery',
        # back_populates='purposes')
        
    class PersonalData(db.Model):
        id = db.Column(db.Integer(), primary_key=True)
        name = db.Column(db.String(50), unique=True)

    class Consent(db.Model):
        id = db.Column(db.Integer(), primary_key=True)
        user_id = db.Column(db.Integer, db.ForeignKey('{{- sm.userClass | lower}}.id'), nullable=False)
        user = db.relationship('{{- sm.userClass}}', foreign_keys=user_id, backref='consents')
        data_id = db.Column(db.Integer, db.ForeignKey('personal_data.id'), nullable=False)
        data = db.relationship('PersonalData', foreign_keys=data_id)
        purposes = db.relationship('Purpose', secondary=consentedpurposes, lazy='subquery')#, back_populates='consents')


