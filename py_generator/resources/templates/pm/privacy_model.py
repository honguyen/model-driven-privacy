from model import Action
from flask_user import current_user

class PrivacyModel:

    # Extensible model (default: nothing declared)
    model = []

    # Checks permissions over the role hirearchy 
    @classmethod
    def permit(cls, aps, attr, act, self, caller, value=None):
        def __privacycheck__():
            data = self.__class__.__name__
            if act == Action.create or act == Action.delete:
                return cls.check1(aps, data, act, caller)
            if act == Action.read:
                return cls.check2(aps, data, attr, act, caller, self)
            return cls.check3(aps, data, attr, act, caller, self, value)  
        return __privacycheck__()

    def class_check(ls,data):
        for l in ls:
            if l["class"] == data:
                return True
        return False
    
    def resource_check(ls, data, attr):
        for l in ls:
            a = l.get("attribute",l.get("ends"))
            if l["class"] == data and attr == a:
                return True
        return False

    @classmethod
    def check1(cls, ap, data, act, caller):
        model = list(filter(lambda dp: PrivacyModel.class_check(dp[2], data) and dp[3](caller=caller), cls.model))
        dps = list(map(lambda dp: dp[0].name,model))
        consents = [c for c in current_user.consents if c.data.name == data]
        return cls.check(ap,dps,consents)


    @classmethod
    def check2(cls, ap, data, attr, act, caller, self):
        model = list(filter(lambda dp: PrivacyModel.resource_check(dp[2], data, attr) and dp[3](caller=caller,self=self), cls.model))
        dps = list(map(lambda dp: dp[0].name,model))
        consents = [c for c in self.owner.consents if c.data.name == data]
        return cls.check(ap,dps,consents)

    @classmethod
    def check3(cls, ap, data, attr, act, caller, self, value):
        model = list(filter(lambda dp: PrivacyModel.resource_check(dp[2], data, attr) and dp[3](caller=caller,self=self, value=value), cls.model))
        dps = list(map(lambda dp: dp[0].name,model))
        consents = [c for c in self.owner.consents if c.data.name == data]
        return cls.check(ap,dps,consents)
    
    @classmethod
    def check(cls,ap,dps,consents):
        check_dp = set(ap).issubset(set(dps))
        cps = [p.name for c in consents for p in c.purposes]
        check_cn = set(ap).issubset(set(cps))
        return check_dp and check_cn
        

    #TODO: check well-formdness of the model
    @classmethod
    def validate(cls):
        return True
