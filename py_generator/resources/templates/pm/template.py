from model import Action, Constraint
from enum import auto, Enum
from privacy_model import PrivacyModel


class {{projectname}}PrivacyModel(PrivacyModel):

    # Extensible model (default: nothing declared)

    class Purpose(Enum):
        {% for p in pm.purposes -%}
        {{ p.name }} = auto()
        {% endfor %}
        
    model = {{policy | replace('\'Action.read\'','Action.read') |
                        replace('\'Action.add\'','Action.add') |
                        replace('\'Action.remove\'','Action.remove') |
                        replace('\'Action.update\'','Action.update') |
                        replace('\'Action.create\'','Action.create') |
                        replace('\'Action.delete\'','Action.delete') |
                        replace('\'Constraint.fullAccess\'','Constraint.fullAccess') | 
                        replace('\'Constraint.noAccess\'','Constraint.noAccess') | 
                        r | rrr }}
