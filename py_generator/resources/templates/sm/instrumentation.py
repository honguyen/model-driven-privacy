from flask import render_template, session
from flask_user import current_user
from stm import {{projectname}}
from model import Action
import inspect



class SecurityException(Exception):
    def __init__(self, msg = 'Not allowed by the securty model', page = 'error.html', params = {}):
        self.msg = msg
        self.page = page
        self.params = params

class PrivacyException(Exception):
    def __init__(self, msg = 'Not allowed by the privacy model', page = 'error.html', params = {}):
        self.msg = msg
        self.page = page
        self.params = params

def get_context():
    def __securecontext__():
        roles = {
            {% for r in sm.roles -%} 
            '{{- r.name}}': {{projectname}}.Role.{{- r.name}},
            {% endfor -%}
        }
        c = current_user if not current_user.is_anonymous else None
        r = c.role if not c is None else 'Visitor'
        return (c,roles[r.name])
    return __securecontext__()

def secure(db,ps):
    def f(func):
        def __securedentrypoint__(*args, **kwargs):
            pss = list(map(lambda p: p.get_subpurposes(), ps()))
            try:
                if 'purpose' not in session:
                    session['purpose'] = []
                for p in pss:
                    session['purpose'].append(p)
                return func(*args, **kwargs)
            except (SecurityException, PrivacyException) as se:
                db.session.rollback()
                return render_template(se.page, security_violation = True, msg = se.msg, **se.params)
            finally:
                for i in range(len(pss)):
                    session['purpose'].pop()

        __securedentrypoint__.__name__ = func.__name__
        return __securedentrypoint__
    return f

def check_context(c):
    return '__securedentrypoint__' in c and '__securitycheck__' not in c and '__privacycheck__' not in c and '__securecontext__' not in c and 'orm_setup_cursor_result' not in c and '_initialize_instance' not in c

class SecuredAttribute():
        
    def __init__(self, attr, resource, sm, pm):
        self._attr = attr
        self._resource = resource
        self._sm = sm
        self._pm = pm

    def __getattr__(self, attr):
        return getattr(self._attr, attr)

    def __set__(self, instance, value):
        if not isinstance(value, list):
            context = list(map(lambda x: x.function, inspect.getouterframes(inspect.currentframe())))
            if check_context(context):    
                resource = instance
                caller, role = get_context()
                if hasattr(self._attr,'name'):
                    attr = self._attr.name 
                else:
                    attr = self._attr.__str__().split(".")[-1]
                purpose = session['purpose'] if 'purpose' in session else []
                if self._sm.permit(role,attr,Action.update,resource,caller,value):
                    if self._pm.permit(purpose,attr,Action.update,resource,caller,value):
                        self._attr.__set__(instance,value)
                    else:
                        resource = resource.__class__.__name__
                        raise PrivacyException(msg = f"It is not allowed to use '{attr}' attribute of the '{resource}' class for '{purpose}' purposes")

                else:
                    role = caller.role if not caller.is_anonymous else "Visitor"
                    caller = "User '" + caller.username + "'" if not caller.is_anonymous else "Anonymous user"
                    role = role.name
                    resource = resource.__class__.__name__
                    raise SecurityException(msg = f"{caller} with '{role}' role is not allowed to update '{attr}' attribute of the '{resource}' class to '{value}' value")
            else:
                self._attr.__set__(instance,value) # SYSTEM actions - do not check
        else:
            self._attr.__set__(instance,value)


    def __delete__(self, instance):
        self._attr.__delete__(instance)

    def __get__(self, instance, owner):
        context = list(map(lambda x: x.function, inspect.getouterframes(inspect.currentframe())))
        if check_context(context):
            resource = instance
            caller, role = get_context()
            ret = self._attr.__get__(instance,owner)
            if isinstance(ret, list):
                attr = self._attr._annotations['proxy_key'] if hasattr(self._attr,'_annotations') else self._attr.fget.__name__
                purpose = session['purpose'] if 'purpose' in session else []
                if self._sm.permit(role, attr, Action.read, resource, caller):
                    if self._pm.permit(purpose, attr, Action.read, resource, caller):
                        return SecuredList(ret, self._attr, resource, self._sm, self._pm)
                    else:
                        resource = resource.__class__.__name__
                        raise PrivacyException(msg = f"It is not allowed to use '{attr}' attribute of the '{resource}' class for '{purpose}' purposes")
                else:
                    role = caller.role if not caller.is_anonymous else "Visitor"
                    caller = "User '" + caller.username + "'" if not caller.is_anonymous else "Anonymous user"
                    role = role.name
                    resource = resource.__class__.__name__
                    raise SecurityException(msg = f"{caller} with '{role}' role is not allowed to read '{attr}' attribute of the '{resource}' class")
            else:
                if hasattr(self._attr,'name'):
                    attr = self._attr.name 
                else:
                    attr = self._attr.__str__().split(".")[-1]
                purpose = session['purpose'] if 'purpose' in session else []
                if self._sm.permit(role, attr, Action.read, resource, caller):
                    if self._pm.permit(purpose, attr, Action.read, resource, caller):
                        return ret
                    else:
                        resource = resource.__class__.__name__
                        raise PrivacyException(msg = f"It is not allowed to use '{attr}' attribute of the '{resource}' class for '{purpose}' purposes")
                else:
                    role = caller.role if not caller.is_anonymous else "Visitor"
                    caller = "User '" + caller.username + "'" if not caller.is_anonymous else "Anonymous user"
                    role = role.name
                    resource = resource.__class__.__name__
                    raise SecurityException(msg = f"{caller} with '{role}' role is not allowed to read '{attr}' attribute of the {resource} class")
        else:
            return self._attr.__get__(instance,owner) # SYSTEM actions - do not check
        

class SecuredList():

    def __init__(self, list, attr, instance, sm, pm):
        self._list = list
        self._attr = attr
        self._instance = instance
        self._sm = sm
        self._pm = pm
    
    def __getattr__(self, attr):
        return getattr(self._list, attr)

    def __repr__(self) -> str:
        return f'{self._list}'

    def append(self, value):
        context = list(map(lambda x: x.function, inspect.getouterframes(inspect.currentframe())))
        if check_context(context):
            resource = self._instance
            caller, role = get_context()
            attr = self._attr._annotations['proxy_key'] if hasattr(self._attr,'_annotations') else self._attr.fget.__name__
            purpose = session['purpose'] if 'purpose' in session else []
            if self._sm.permit(role,attr,Action.add,resource,caller,value):
                if self._pm.permit(purpose,attr,Action.add,resource,caller,value):
                    self._list.append(value)
                else:
                    resource = resource.__class__.__name__
                    raise PrivacyException(msg = f"It is not allowed to use '{attr}' attribute of the '{resource}' class for '{purpose}' purposes")
            else:
                role = caller.role if not caller.is_anonymous else "Visitor"
                caller = "User '" + caller.username + "'" if not caller.is_anonymous else "Anonymous user"
                role = role.name
                resource = resource.__class__.__name__
                raise SecurityException(msg = f"{caller} with '{role}' role is not allowed to add '{value}' value to '{attr}' attribute of the '{resource}' class")    
        else:
            self._list.append(value) # SYSTEM actions - do not check

    def remove(self, value):
        context = list(map(lambda x: x.function, inspect.getouterframes(inspect.currentframe())))
        if check_context(context):
            resource = self._instance
            caller, role = get_context()
            attr = self._attr._annotations['proxy_key'] if hasattr(self._attr,'_annotations') else self._attr.fget.__name__
            purpose = session['purpose'] if 'purpose' in session else []
            if self._sm.permit(role,attr,Action.remove,resource,caller,value):
                if self._pm.permit(purpose,attr,Action.remove,resource,caller,value):
                    self._list.remove(value)
                else:
                    resource = resource.__class__.__name__
                    raise PrivacyException(msg = f"It is not allowed to use '{attr}' attribute of the '{resource}' class for '{purpose}' purposes")
            else:
                role = caller.role if not caller.is_anonymous else "Visitor"
                caller = "User '" + caller.username + "'" if not caller.is_anonymous else "Anonymous user"
                role = role.name
                resource = resource.__class__.__name__
                raise SecurityException(msg = f"{caller} with '{role}' role is not allowed to remove '{value}' value from '{attr}' attribute of the '{resource}' class")
        else:
            self._list.remove(value) # SYSTEM actions - do not check

class Secure:
    resources = {{resources}}

    def __init__(self,sm,pm):
        self._sm = sm
        self._pm = pm
    def __call__(self,data):
        sm = self._sm
        pm = self._pm
        toProtect = Secure.resources[data.__name__]
        for attr in toProtect:
            orig_attr = getattr(data,attr)
            new_attr = SecuredAttribute(orig_attr,data,sm,pm)
            setattr(data,attr,new_attr)     
        return data
