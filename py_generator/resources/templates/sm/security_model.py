from model import Action, Constraint

class SecurityModel:

    # Extensible set of Roles and their hierarchy
    closure = {}
    
    @classmethod
    def close(cls,rs):
        pairs = set([(x,y) for x in rs for y in rs if x.isSubRole(y)])
        relations = {}
        for x,y in pairs:
            if x not in relations:
                relations[x] = []
            relations[x].append(y)
        
        closure = {} 
        def build_closure(n):
            def f(k):
                for y in relations.get(k, []):
                    if n not in closure:
                        closure[n] = set()
                    closure[n].add(y)
                    f(y)
            f(n)

        for k in relations.keys():
            build_closure(k)
        
        for r in rs:
            if r not in closure:
                closure[r] = set()
            closure[r].add(r)

        cls.closure = closure

    # Syntactic sugar
    create = { Action.create: Constraint.createAccess }
    delete = { Action.delete: Constraint.deleteAccess }
    read = { Action.read: Constraint.readAccess }
    update = { Action.update: Constraint.updateAccess }
    add = { Action.add: Constraint.addAccess }
    remove = { Action.remove: Constraint.removeAccess }

    # Extensible model (default: deny all)
    model = {}

    # Checks permissions over the role hirearchy 
    @classmethod
    def permit(cls, r, attr, act, self, caller, value=None):
        def __securitycheck__():
            data = self.__class__.__name__
            for role in cls.Role:
                if role <= r:
                    if act == Action.create or act == Action.delete:
                        if cls.check(role, data, attr, act)(caller=caller):
                            return True
                    if act == Action.read:
                        if cls.check(role, data, attr, act)(caller=caller, self=self):
                            return True
                    elif cls.check(role, data, attr, act)(caller=caller, self=self, value=value):
                        return True
            return False
        return __securitycheck__()
     
    # Returns relevant constraints according to the model semantics 
    # Params: role: role enum, data: data model class name, attr: attribute name, act: action enum
    # Returns a function from the model that represents authorization constraint 
    @classmethod
    def check(cls, role, data, attr, act):
        model = cls.model
        if role in model:
            model = model[role]
            if data in model:
                model = model[data]
                if attr in model:
                    model = model[attr]
                    if act in model:
                        return model[act]
                    else:
                        # No specifics on the action
                        if model is callable:
                            return model
                        return Constraint.noAccess
                else:
                    # No specifics on the attribute of a class
                    if act in model:
                        return model[act]
                    else:
                        # No specifics on the action
                        if model is callable:
                            return model
                        return Constraint.noAccess
            else:
                # No specifics on the class
                if attr in model:
                    model = model[attr]
                    if act in model:
                        return model[act]
                    else:
                        # No specifics on the action
                        if model is callable:
                            return model
                        return Constraint.noAccess
                else:
                    # No specifics on the attribute of a class
                    if act in model:
                        return model[act]
                    else:
                        # No specifics on the action
                        if model is callable:
                            return model
                        return Constraint.noAccess
        else:
            # No role in the model => deny
            return Constraint.noAccess

    #TODO: check well-formdness of the model
    @classmethod
    def validate(cls):
        return True
