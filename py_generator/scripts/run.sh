#!/bin/bash

PROJECTNAME="ConfMS"

# Initialize project

cd ../output
rm -rf ${PROJECTNAME}
mkdir ${PROJECTNAME}
cd ${PROJECTNAME}


#Installing packages
cp ../../scripts/requirements.txt . 
python3 -m venv .venv
. .venv/bin/activate
pip3 install -r requirements.txt

# Generate all
# Copy/Overwrite generated Models into the projects
cd ../../scripts
python3 transformation.py -p ${PROJECTNAME}

# Build and run
# cd ../output/${PROJECTNAME}
# flask --app app.py run --host=0.0.0.0
