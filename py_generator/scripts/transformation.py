#!/usr/bin/env python3
"""
@author: Anonymous Author(s)
"""

import os
import shutil
import json
import re
from jinja2 import Environment, FileSystemLoader, select_autoescape

"""
Simple Python generator for privacy enhanced web applications
"""
BASE_DIRECTORY = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

"""
JSON parser
"""
class JSONObject(object):
    def __init__(self, d):
        self.__dict__ = d

"""
Directory navigation
"""
def clean_dir(*path):
    dir = os.path.join(BASE_DIRECTORY, *path)
    if os.path.exists(dir):
        shutil.rmtree(dir)
    os.mkdir(dir)

def set_working_directory(*path):
    dir = os.path.join(BASE_DIRECTORY, *path)
    os.chdir(dir)

"""
Auxiliary functions
"""

types = {"String":'db.String(100, collation="NOCASE")',
         "Boolean":'db.Boolean',
         "Integer":'db.Integer',
         "None": "NoneType"}

def type(t):
    """
    Get Python corresponding types
    """
    if t in types:
        return types[t]
    return t

def isPersonalData(c, pm):
    """
    return True if c is a personalData w.r.t. model pm
    """
    for p in pm['personalData']:
        if p == c['class']:
            return True
    return False

def label(c,met,pm):
    """
    label actual purposes to method met of class c w.r.t. model pm
    """
    # TODO: Extend with the multiple purposes
    for p in pm['purposes']:
        for end in p['endpoints']:
            if end['class'] == c['class'] and end['met'] == met:
                return f"(purpose=\"p['name']\")"
    return ""

def isUserClass(c,sm):
    uc = sm.get('userClass',"User")
    return c['class'] == uc


### Main generator



def generate_class(projectname,dm,sm,pm):
    """
    Generate class
    """
    class_file = os.path.join(BASE_DIRECTORY, "output", projectname, f"dtm.py")
    app_file = os.path.join(BASE_DIRECTORY, "output", projectname, f"app.py")
    application_file = os.path.join(BASE_DIRECTORY, "output", projectname, f"application.py")

    set_working_directory("resources", "templates", "dm")
    env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )

    t = env.get_template("template.py")
    t.globals['type'] = type
    t.globals['label'] = label
    t.globals['isPersonalData'] = isPersonalData
    t.globals['isUserClass'] = isUserClass

    render = t.render(projectname=projectname,dm=dm,sm=sm,pm=pm)

    with open(class_file,"w+",encoding="utf-8") as f:
        f.write(render)


    def get_purpose(m):
        ret = {}
        for p in pm["purposes"]:
            for e in p["endpoints"]:
                if e["met"] not in ret:
                    ret[e["met"]] = []
                ret[e["met"]].append(p["name"])
        return ret.get(m,[])
    
    t = env.get_template("app.py")
    t.globals['type'] = type
    t.globals['label'] = label
    t.globals['isPersonalData'] = isPersonalData
    t.globals['isUserClass'] = isUserClass
    t.globals['get_purpose'] = get_purpose

    render = t.render(projectname=projectname,dm=dm,sm=sm,pm=pm)

    with open(app_file,"w+",encoding="utf-8") as f:
        f.write(render)

    t = env.get_template("application.py")

    render = t.render()

    with open(application_file,"w+",encoding="utf-8") as f:
        f.write(render)

aAuth = {
    "true"  : "Constraint.fullAccess",
    "false" : "Constraint.noAccess",
    "self = caller" : "lambda caller, self, *x, **y: caller == self",
    "self.reviews->size() < 3" : "lambda caller, self, *x, **y: len(self.reviews) < 3",
    "caller.follows->includes(self)" : "lambda caller, self, *x, **y: self in caller.follows",
    "self.student" : "lambda caller, self, *x, **y: self.student"
}


def refactor(ps,dm):

    def rMap(r):
        return f"Role.{r}"

    aMap = {
        "read"  : "Action.read",
        "update" : "Action.update",
        "create": "Action.create",
        "delete": "Action.delete"
    }

    aAsc = {
        "read"  : "Action.read",
        "create": "Action.add",
        "delete": "Action.remove"
    }

    

    def isClass(r):
        return list(r.keys()) == ["class"]

    def isAttribute(r):
        return "attribute" in r.keys()

    def isAssociation(r):
        return list(r.keys()) == ["association"]


    model = {}

    for p in ps:
        r = rMap(p["role"])

        if r not in model:
            model[r] = {}
        
        if isClass(p["resource"]):
            if p["resource"]["class"] not in model[r]:
                model[r][p["resource"]["class"]] = {}
            model[r][p["resource"]["class"]][aMap[p["action"]]] = aAuth[p["auth"]]

        elif isAttribute(p["resource"]):
            if p["resource"]["class"] not in model[r]:
                model[r][p["resource"]["class"]] = {}
            if p["resource"]["attribute"] not in model[r][p["resource"]["class"]]:
                model[r][p["resource"]["class"]][p["resource"]["attribute"]] = {}
            model[r][p["resource"]["class"]][p["resource"]["attribute"]][aMap[p["action"]]] = aAuth[p["auth"]]

        elif isAssociation(p["resource"]):
            ac = list(filter(lambda d: "isAssociation" in d and d["class"] == p["resource"]["association"], dm))
            assert(len(ac) == 1)
            ac = ac[0]
            c1 = ac["ends"][0]
            c2 = ac["ends"][1]
            if c1["target"] not in model[r]:
                model[r][c1["target"]] = {}
            if c2["name"] not in model[r][c1["target"]]:
                model[r][c1["target"]][c2["name"]] = {}
            model[r][c1["target"]][c2["name"]][aAsc[p["action"]]] = aAuth[p["auth"]]

            if c2["target"] not in model[r]:
                model[r][c2["target"]] = {}
            if c1["name"] not in model[r][c2["target"]]:
                model[r][c2["target"]][c1["name"]] = {}
            model[r][c2["target"]][c1["name"]][aAsc[p["action"]]] = aAuth[p["auth"]]

        # else:
        #     print("internal error: " + str(p["resource"]))

    return model

def get_resources(dm):
    resources = {}
    for c in dm:
        if "isAssociation" in c:
            c0 = c["ends"][0]["target"]
            c1 = c["ends"][1]["target"]
            if c0 not in resources:
                resources[c0] = []
            resources[c0] += [c["ends"][1]["name"]]
            if c1 not in resources:
                resources[c1] = []
            resources[c1] += [c["ends"][0]["name"]]

        else:
            if c["class"] not in resources:
                resources[c["class"]] = []
            resources[c["class"]] += list(map(lambda a: a["name"],c["attributes"]))

    return resources

def r(text):
    return re.sub('\'lambda(.*?)\'', 'lambda\\1', text)

def rr(text):
    return re.sub('\'Role\.([_a-zA-Z][_a-zA-Z0-9]*)\'', 'Role.\\1', text)

def rrr(text):
    return re.sub('\'Purpose\.([_a-zA-Z][_a-zA-Z0-9]*)\'', 'Purpose.\\1', text)

def set_up_security(projectname):
    filenames = ['instrumentation', 'security_model', 'template']

    set_working_directory("resources", "templates", "sm")
    env = Environment(
            loader=FileSystemLoader("."),
            autoescape=select_autoescape()
        )
    env.filters['r'] = r
    env.filters['rr'] = rr
    

    for filename in filenames:
        target = os.path.join(BASE_DIRECTORY, "output", projectname, f"stm.py") if filename == "template" else os.path.join(BASE_DIRECTORY, "output", projectname, f"{filename}.py")

        policy = refactor(sm["policy"], dm) if filename == "template" else ""
        resources = get_resources(dm) if filename == "instrumentation" else ""
        
        t = env.get_template(f"{filename}.py")
        
        render = t.render(projectname=projectname,dm=dm,pm=pm,sm=sm,policy=policy,resources=resources)
        with open(target,"w+",encoding="utf-8") as f:
            f.write(render)

    

def set_up_aux(projectname,dm):
    
    set_working_directory("resources", "templates", "auxiliary")
    
    application_user_target = os.path.join(BASE_DIRECTORY, "output", projectname)

    shutil.copy('model.py', os.path.join(application_user_target, 'model.py'))
    shutil.copytree('templates', os.path.join(application_user_target,'templates'))
    shutil.copytree('static', os.path.join(application_user_target,'static'))
    
    env = Environment(
            loader=FileSystemLoader("."),
            autoescape=select_autoescape()
    )
    t = env.get_template(os.path.join("templates", "template.html"))
    
    render = t.render(projectname=projectname)
    target = os.path.join(application_user_target,'templates', "template.html")
    with open(target,"w+",encoding="utf-8") as f:
        f.write(render)

    t = env.get_template(os.path.join("templates", "main.html"))
    
    flatten = lambda x: [y for z in x for y in z]
    endpoints = flatten(list(map(lambda c: list(map(lambda d:d["name"] ,c["methods"])),list(filter(lambda c: "methods" in c, dm)))))
    endpoints = list(map(lambda e: "    <a class=\"list-group-item list-group-item-action\" href={{ url_for('" + e + "') }}>" + e + "</a>",endpoints))
    render = t.render(projectname=projectname,endpoints=endpoints)
    target = os.path.join(application_user_target,'templates', "main.html")
    with open(target,"w+",encoding="utf-8") as f:
        f.write(render)

        

def refactor_privacy(pm):
    ret = []
    for l in pm["policy"]:
        ocl = l["constraint"] if "ocl" not in l["constraint"] else l["constraint"]["ocl"]
        desc = l["constraint"]["desc"] if "desc" in l["constraint"] else ocl
        act = None
        ret.append(("Purpose."+l["purpose"],act,l["resources"],aAuth[ocl],desc))
    return ret


def set_up_privacy(projectname,pm):
    # filenames = ['Consent', 'Purpose', 'PersonalData', 'Ownership']

    set_working_directory("resources", "templates", "pm")
    application_user_target = os.path.join(BASE_DIRECTORY, "output", projectname)

    env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )
    env.filters['r'] = r
    env.filters['rrr'] = rrr

    # for filename in filenames:
    #     target = os.path.join(BASE_DIRECTORY, "output", projectname, "Models", f"{filename}.cs")
    #     t = env.get_template(f"{filename}.cs")
    #     render = t.render(projectname=projectname,
    #                       sm=sm, 
    #                       pm=pm,
    #                       dm=dm)
    #     with open(target,"w+",encoding="utf-8") as f:
    #         f.write(render)

    t = env.get_template("privacy_model.py")
    target = os.path.join(application_user_target, "privacy_model.py")
        
    render = t.render(projectname=projectname,dm=dm,pm=pm,sm=sm)
    with open(target,"w+",encoding="utf-8") as f:
        f.write(render)

    t = env.get_template("template.py")
    target = os.path.join(application_user_target, "ptm.py")

    policy = refactor_privacy(pm)
        
    render = t.render(projectname=projectname,dm=dm,pm=pm,sm=sm, policy=policy)
    with open(target,"w+",encoding="utf-8") as f:
        f.write(render)


def set_up_aspect(projectname, sm, pm):
    set_working_directory("resources", "templates", "pm")

    filenames = ['PersonalDataAttribute', 'PrivacyException', 'PrivacyEnforcer']
    
    env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )

    clean_dir("output", projectname, "Privacy")
    for filename in filenames:
        target = os.path.join(BASE_DIRECTORY, "output", projectname, "Privacy", f"{filename}.cs")
        t = env.get_template(f"{filename}.cs")
        render = t.render(projectname=projectname,
                          sm=sm, 
                          pm=pm,
                          dm=dm)
        with open(target,"w+",encoding="utf-8") as f:
            f.write(render)

def set_up_consent_collect_page(projectname,sm,pm):

    set_working_directory("resources", "templates", "pm")

    clean_dir("output", projectname, "Views", "CollectConsent")
    view_target = os.path.join(BASE_DIRECTORY, "output", projectname, "Views", "CollectConsent", "Index.cshtml")
    env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )
    t = env.get_template("Index.cshtml")
    render = t.render(projectname=projectname)
    with open(view_target,"w+",encoding="utf-8") as f:
        f.write(render)

    controller_target = os.path.join(BASE_DIRECTORY, "output", projectname, "Controllers", "CollectConsentController.cs")
    env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )
    t = env.get_template("CollectConsentController.cs")
    render = t.render(projectname=projectname,sm=sm)
    with open(controller_target,"w+",encoding="utf-8") as f:
        f.write(render)

    model_target = os.path.join(BASE_DIRECTORY, "output", projectname, "Models", "Policy.cs")
    env = Environment(
        loader=FileSystemLoader("."),
        autoescape=select_autoescape()
    )
    t = env.get_template("Policy.cs")
    render = t.render(projectname=projectname,pm=pm)
    with open(model_target,"w+",encoding="utf-8") as f:
        f.write(render)
   
def generate_project(projectname,dm,sm,pm):
    """
    Code generation from models
    """
    set_up_security(projectname)
    set_up_privacy(projectname,pm)
    set_up_aux(projectname,dm)
    generate_class(projectname,dm,sm,pm)
    # set_up_consent_collect_page(projectname,sm,pm)

if __name__ == "__main__":
    
    print("Running parser with root directory " + BASE_DIRECTORY)

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-p','--projectname', help='project name', required=True)              
    args = parser.parse_args()

    set_working_directory("models", args.projectname)
    with open("dm.json", "r") as f:
        dm = json.load(f)
    with open("sm.json", "r") as f:
        sm = json.load(f)
    with open("pm.json", "r") as f:
        pm = json.load(f)

    # Add visitor role
    sm["roles"].append({"name" : "Visitor"})

    clean_dir("output", args.projectname)
    generate_project(args.projectname,dm,sm,pm)
